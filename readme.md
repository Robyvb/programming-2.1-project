# Programming 2.1 - Personal project Roby Van Boxelaere 2ACS
This is my personal project that we had to make for the Programming 2.1 course. It is written in JDK version 11.

## Domain classes
The project contains three domain classes:
- Developer
- Game
- Platform

The relationships are:
- A game can have multiple developers and multiple platforms (2 many to many)
- A developer can have multiple games (1 many to many)
- A platform can have multiple games (1 many to many)

## Project structure
For each week of assignments, I created a new project.
The projects for the end of each sprints are:
- Week 1&2: Sprint 1
- Week 4: Sprint 2
- Week 6: Sprint 3
- Week 9: Sprint 4
- Week 11: Final version

## Completed assignments
Most of the things asked are included. I ran into some problems at week 9 and had to continue with some problems to week 11 (although I did do everything asked in week 10, 11 because the problems didn't interfere with what was asked.). 
- Sprint 1: Fully completed
- Sprint 2: Fully completed
- Sprint 3: Fully completed
- Sprint 4: Adding entities broke, this was a requirement in sprint 3. When adding developers, one gets added for each game that was selected when adding them. This makes it so multiple developers get created. Adding games doesn't work, I turned off adding the developers and platforms to the game so it didn't produce an error.
- Final version: Again, adding doesn't work. Everything that was asked to implement in these two weeks was added.