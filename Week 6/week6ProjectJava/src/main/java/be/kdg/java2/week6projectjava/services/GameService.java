package be.kdg.java2.week6projectjava.services;

import be.kdg.java2.week6projectjava.domain.Game;

import java.util.List;

public interface GameService {
    void saveGamesToJson(List<Game> games);
    List<Game> showAllGames();
    Game addGame(Game game);
    Game findGameById(int id);
}
