package be.kdg.java2.week6projectjava.domain;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Game extends Entity {
    private String name;
    private double rating;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate releaseDate;
    private List<GameGenres> genres;

    transient private List<Platform> platforms;
    transient private List<Developer> developers;

    public String getName() {
        return name;
    }

    public double getRating() {
        return rating;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public List<Platform> getPlatforms() {
        return platforms;
    }

    public List<Developer> getDevelopers() {
        return developers;
    }

    public List<String> getDeveloperNames(){
        List<String> developerNames = new ArrayList<>();
        developers.forEach(developer -> developerNames.add(developer.getName()));
        return developerNames;
    }

    public List<GameGenres> getGenres() {
        return genres;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setGenres(List<GameGenres> genres) {
        this.genres = genres;
    }

    public void setPlatforms(List<Platform> platforms) {
        this.platforms = platforms;
    }

    public void setDevelopers(List<Developer> developers) {
        this.developers = developers;
    }

    public void addGenres(List<GameGenres> genres){
        this.genres.addAll(genres);
    }

    public void addDevelopers(List<Developer> developers){
        this.developers.addAll(developers);
    }

    @Override
    public String toString() {
        return name;
    }

    public Game(String name, double rating, LocalDate releaseDate, List<GameGenres> genres, List<Platform> platforms) {
        this.name = name;
        this.rating = rating;
        this.releaseDate = releaseDate;
        this.genres = genres;
        this.platforms = platforms;
    }

    public Game(String name, double rating, LocalDate releaseDate, List<GameGenres> genres, List<Platform> platforms, List<Developer> developers) {
        //Release date can be in the future
        this.name = name;
        this.rating = rating;
        this.releaseDate = releaseDate;
        this.genres = genres;
        this.platforms = platforms;
        this.developers = developers;
    }
}