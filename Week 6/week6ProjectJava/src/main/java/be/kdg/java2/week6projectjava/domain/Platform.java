package be.kdg.java2.week6projectjava.domain;

import java.util.ArrayList;
import java.util.List;

public class Platform extends Entity {
    private String platformName;
    private String abbreviation;
    private String platformMaintainer;
    private int version;

    transient private List<Game> games = new ArrayList<>();

    public String getPlatformName() {
        return platformName;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public String getPlatformMaintainer() {
        return platformMaintainer;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setPlatformMaintainer(String platformMaintainer) {
        this.platformMaintainer = platformMaintainer;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void addGames(List<Game> games){
        this.games.addAll(games);
    }

    @Override
    public String toString() {
        return platformName;
    }

    public Platform(String platformName, String abbreviation, String platformMaintainer, int version) {
        //no negative versions
        if(version >= 0){
            this.platformName = platformName;
            this.abbreviation = abbreviation;
            this.platformMaintainer = platformMaintainer;
            this.version = version;
        }
    }
}
