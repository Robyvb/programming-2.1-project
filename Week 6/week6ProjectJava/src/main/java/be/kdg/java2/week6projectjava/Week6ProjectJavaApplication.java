package be.kdg.java2.week6projectjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week6ProjectJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(Week6ProjectJavaApplication.class, args);
    }

}
