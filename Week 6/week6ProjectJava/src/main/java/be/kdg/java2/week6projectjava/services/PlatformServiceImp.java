package be.kdg.java2.week6projectjava.services;

import be.kdg.java2.week6projectjava.domain.Platform;
import be.kdg.java2.week6projectjava.repository.JSONSaver;
import be.kdg.java2.week6projectjava.repository.PlatformRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PlatformServiceImp implements PlatformService{
    private PlatformRepository repository;
    private JSONSaver jsonSaver;
    private static final Logger logger = LoggerFactory.getLogger(PlatformServiceImp.class);

    @Autowired
    public PlatformServiceImp(PlatformRepository repository, JSONSaver jsonSaver) {
        this.repository = repository;
        this.jsonSaver = jsonSaver;
    }

    @Override
    public void savePlatformsToJson(List<Platform> platforms) {
        jsonSaver.savePlatformsToJson(platforms);
    }

    @Override
    public List<Platform> showAllPlatforms() {
        return repository.read();
    }

    @Override
    public Platform addPlatform(Platform platform) {
        return repository.create(platform);
    }

    @Override
    public Platform findPlatformById(int id) {
        logger.debug("Finding platform by id " + id);
        return repository.findById(id);
    }
}
