package be.kdg.java2.week6projectjava.repository;

import be.kdg.java2.week6projectjava.domain.Entity;

import java.util.List;

public interface EntityRepository<T extends Entity>{
    List<T> read();

    T create(T t);

    T findById(int id);

    void delete(T t);

    void update(T t);
}
