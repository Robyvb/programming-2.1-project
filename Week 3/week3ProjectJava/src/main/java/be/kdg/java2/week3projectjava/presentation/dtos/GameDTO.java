package be.kdg.java2.week3projectjava.presentation.dtos;

import be.kdg.java2.week3projectjava.domain.GameGenres;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

public class GameDTO {
    private String name;
    private double rating;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate releaseDate;
    private List<GameGenres> genres;

    transient List<Integer> platforms;
    transient List<Integer> developers;

    public GameDTO(String name, double rating, LocalDate releaseDate) {
        this.name = name;
        this.rating = rating;
        this.releaseDate = releaseDate;
    }

    public String getName() {
        return name;
    }

    public double getRating() {
        return rating;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public List<GameGenres> getGenres() {
        return genres;
    }

    public void setGenres(List<GameGenres> genres) {
        this.genres = genres;
    }

    public List<Integer> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<Integer> platforms) {
        this.platforms = platforms;
    }

    public List<Integer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<Integer> developers) {
        this.developers = developers;
    }

    @Override
    public String toString() {
        return "GameDTO{" +
                "name='" + name + '\'' +
                ", rating=" + rating +
                ", releaseDate=" + releaseDate +
                ", genres=" + genres +
                ", platforms=" + platforms +
                ", developers=" + developers +
                '}';
    }
}
