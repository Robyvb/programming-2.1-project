package be.kdg.java2.week3projectjava.repository;

import be.kdg.java2.week3projectjava.domain.Developer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DeveloperRepository extends ListRepository<Developer>{
    private Logger logger = LoggerFactory.getLogger(DeveloperRepository.class);

    public DeveloperRepository() {
        logger.debug("Creating developer repository");
    }


}
