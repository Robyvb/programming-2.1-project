package be.kdg.java2.week3projectjava.services;

import be.kdg.java2.week3projectjava.domain.Developer;
import be.kdg.java2.week3projectjava.domain.Game;
import be.kdg.java2.week3projectjava.repository.DeveloperRepository;
import be.kdg.java2.week3projectjava.repository.GameRepository;
import be.kdg.java2.week3projectjava.repository.JSONSaver;
import be.kdg.java2.week3projectjava.repository.ListRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class DeveloperServiceImp implements DeveloperService{
    private DeveloperRepository repository;
    private JSONSaver jsonSaver;
    private static final Logger logger = LoggerFactory.getLogger(DeveloperServiceImp.class);
    @Autowired
    public DeveloperServiceImp(DeveloperRepository repository, JSONSaver jsonSaver){
        this.repository = repository;
        this.jsonSaver = jsonSaver;
    }

    public void saveDevelopersJson(List<Developer> developers){
        jsonSaver.saveDevsToJson(developers);
    }

    @Override
    public void saveDevelopersToJson(List<Developer> developers){
        jsonSaver.saveDevsToJson(developers);
    }

    @Override
    public List<Developer> showAllDevelopers() {
        return repository.read();
    }

    @Override
    public List<Developer> showAllDeveloperGames(){
        return repository.read();
    }

    @Override
    public Developer addDeveloper(Developer developer) {
        logger.debug("Adding developer: " + developer.getFirstName());
        return repository.create(developer);
    }

    @Override
    public Developer findDeveloperById(int id){
        logger.debug("Finding developer by id " + id);
        return repository.findById(id);
    }
}
