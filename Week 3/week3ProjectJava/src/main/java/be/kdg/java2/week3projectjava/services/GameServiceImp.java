package be.kdg.java2.week3projectjava.services;

import be.kdg.java2.week3projectjava.domain.Developer;
import be.kdg.java2.week3projectjava.domain.Game;
import be.kdg.java2.week3projectjava.domain.GameGenres;
import be.kdg.java2.week3projectjava.domain.Platform;
import be.kdg.java2.week3projectjava.repository.GameRepository;
import be.kdg.java2.week3projectjava.repository.JSONSaver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class GameServiceImp implements GameService{
    private GameRepository repository;
    private JSONSaver jsonSaver;
    private static final Logger logger = LoggerFactory.getLogger(DeveloperServiceImp.class);
    @Autowired
    //JSON saving goes here too
    public GameServiceImp(GameRepository repository, JSONSaver jsonSaver){
        this.repository = repository;
        this.jsonSaver = jsonSaver;
    }

    @Override
    public void saveGamesToJson(List<Game> games){
        jsonSaver.saveGamesToJson(games);
    }

    @Override
    public List<Game> showAllGames() {
        return repository.read();
    }

    @Override
    public Game addGame(Game game) {
        logger.debug("Adding game:" + game.getName());
        return repository.create(game);
    }

    @Override
    public Game findGameById(int id){
        logger.debug("Finding game by id " + id);
        return repository.findById(id);
    }
}
