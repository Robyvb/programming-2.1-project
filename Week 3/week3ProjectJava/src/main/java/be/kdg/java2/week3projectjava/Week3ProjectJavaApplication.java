package be.kdg.java2.week3projectjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week3ProjectJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(Week3ProjectJavaApplication.class, args);
    }

}
