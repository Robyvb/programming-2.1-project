package be.kdg.java2.week3projectjava.services;

import be.kdg.java2.week3projectjava.domain.Developer;
import be.kdg.java2.week3projectjava.domain.Game;
import be.kdg.java2.week3projectjava.domain.GameGenres;
import be.kdg.java2.week3projectjava.domain.Platform;

import java.time.LocalDate;
import java.util.List;

public interface GameService {
    void saveGamesToJson(List<Game> games);
    List<Game> showAllGames();
    Game addGame(Game game);
    Game findGameById(int id);
}
