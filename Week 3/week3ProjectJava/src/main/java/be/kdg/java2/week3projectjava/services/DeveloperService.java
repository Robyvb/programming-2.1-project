package be.kdg.java2.week3projectjava.services;

import be.kdg.java2.week3projectjava.domain.Developer;

import java.time.LocalDate;
import java.util.List;

public interface DeveloperService {
    void saveDevelopersToJson(List<Developer> developers);
    List<Developer> showAllDevelopers();
    Developer addDeveloper(Developer developer);
    Developer findDeveloperById(int id);
    List<Developer> showAllDeveloperGames();
}
