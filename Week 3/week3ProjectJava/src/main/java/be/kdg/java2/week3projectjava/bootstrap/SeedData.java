package be.kdg.java2.week3projectjava.bootstrap;

import be.kdg.java2.week3projectjava.domain.Developer;
import be.kdg.java2.week3projectjava.domain.Game;
import be.kdg.java2.week3projectjava.domain.GameGenres;
import be.kdg.java2.week3projectjava.domain.Platform;
import be.kdg.java2.week3projectjava.repository.DeveloperRepository;
import be.kdg.java2.week3projectjava.repository.GameRepository;
import be.kdg.java2.week3projectjava.repository.PlatformRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class SeedData implements CommandLineRunner {
    private Logger logger = LoggerFactory.getLogger(SeedData.class);
    private PlatformRepository platformRepository;
    private DeveloperRepository developerRepository;
    private GameRepository gameRepository;

    public SeedData(PlatformRepository platformRepository, DeveloperRepository developerRepository, GameRepository gameRepository) {
        this.platformRepository = platformRepository;
        this.developerRepository = developerRepository;
        this.gameRepository = gameRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        Platform windows = new Platform("Windows","WIN","Microsoft",10);
        Platform playstation = new Platform ("Playstation","PS","Sony",5);
        Platform xbox = new Platform ("XBOX","XB","Microsoft",5);
        Platform switchConsole = new Platform ("Switch", "SW","Nintendo",1);

        Developer gabeNewell = new Developer("Gabe","Newell", LocalDate.of(1962,10,3));
        Developer adamKicinski = new Developer("Adam","Kicinski", LocalDate.of(1969,4,21));
        Developer marcinBlacha = new Developer("Marcin","Blacha", LocalDate.of(1972,11,10));
        Developer marcMerril = new Developer("Marc","Merrill", LocalDate.of(1980,8,17));
        Developer bobbyKotick = new Developer("Bobby","Kotick", LocalDate.of(1963,10,12));
        Developer adamBoyes = new Developer("Adam","Boyes", LocalDate.of(2016,8,8));

        Game CSGO = new Game("Counter-Strike: Global Offensive", 9.5, LocalDate.of(2012,8,21),
                List.of(GameGenres.ACTION,GameGenres.FIRST_PERSON,GameGenres.SHOOTER), List.of(windows),
                List.of(gabeNewell, marcinBlacha));
        Game witcher3 = new Game("The Witcher 3", 9, LocalDate.of(2015,5,18),
                List.of(GameGenres.ACTION,GameGenres.ADVENTURE,GameGenres.FIGHTING,GameGenres.RPG), List.of(playstation, xbox),
                List.of(adamKicinski,marcinBlacha));
        Game LOL = new Game("League Of Legends", 10, LocalDate.of(2009,10,27),
                List.of(GameGenres.FIGHTING, GameGenres.RPG,GameGenres.ACTION), List.of(windows, xbox),
                List.of(marcMerril, gabeNewell, bobbyKotick));
        Game OW = new Game("Overwatch", 8, LocalDate.of(2016,5,24),
                List.of(GameGenres.ACTION,GameGenres.FIRST_PERSON,GameGenres.SHOOTER), List.of(windows, playstation, xbox, switchConsole),
                List.of(bobbyKotick, adamBoyes,adamKicinski));

        //relations
        gabeNewell.setGames(List.of(CSGO, OW));
        adamKicinski.setGames(List.of(witcher3, OW));
        marcinBlacha.setGames(List.of(CSGO, witcher3));
        marcMerril.setGames(List.of(LOL));
        bobbyKotick.setGames(List.of(LOL, OW));
        adamBoyes.setGames(List.of(OW));

        //add all to repositories
        platformRepository.create(windows);
        platformRepository.create(playstation);
        platformRepository.create(xbox);
        platformRepository.create(switchConsole);

        developerRepository.create(gabeNewell);
        developerRepository.create(adamKicinski);
        developerRepository.create(marcinBlacha);
        developerRepository.create(marcMerril);
        developerRepository.create(bobbyKotick);
        developerRepository.create(adamBoyes);

        gameRepository.create(CSGO);
        gameRepository.create(witcher3);
        gameRepository.create(LOL);
        gameRepository.create(OW);
    }
}
