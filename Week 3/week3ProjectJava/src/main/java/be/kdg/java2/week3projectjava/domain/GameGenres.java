package be.kdg.java2.week3projectjava.domain;

public enum GameGenres {
    ACTION("Action"),
    ADVENTURE("Adventure"),
    RPG("RPG"),
    FIGHTING("Fighting"),
    SHOOTER("Shooter"),
    FIRST_PERSON("First person");

    private final String name;

    GameGenres(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
