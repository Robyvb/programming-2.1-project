package be.kdg.java2.week3projectjava.presentation;

import be.kdg.java2.week3projectjava.domain.*;
import be.kdg.java2.week3projectjava.presentation.dtos.GameDTO;
import be.kdg.java2.week3projectjava.presentation.dtos.PlatformDTO;
import be.kdg.java2.week3projectjava.services.DeveloperService;
import be.kdg.java2.week3projectjava.services.GameService;
import be.kdg.java2.week3projectjava.services.PlatformService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/games")
public class GameController {
    private Logger logger = LoggerFactory.getLogger(GameController.class);
    private GameService gameService;
    private DeveloperService developerService;
    private PlatformService platformService;

    public GameController(GameService gameService, DeveloperService developerService, PlatformService platformService) {
        logger.debug("Creating gameController");
        this.gameService = gameService;
        this.developerService = developerService;
        this.platformService = platformService;
    }

    @GetMapping
    public String showAllGames(Model model){
        logger.debug("Running showAllGames");
        model.addAttribute("games", gameService.showAllGames());
        return "games";
    }

    @GetMapping("/add")
    public String addGame(Model model){
        logger.debug("User is adding a game");
        model.addAttribute("genres", GameGenres.values());
        model.addAttribute("developers", developerService.showAllDevelopers());
        //logger.debug("Platforms: " + platformService.showAllPlatforms());
        model.addAttribute("platforms", platformService.showAllPlatforms());
        return "addGame";
    }

    @PostMapping("/add")
    public String processAddGame(GameDTO gameDTO){
        //TODO: finish
        logger.debug("Adding a game: " + gameDTO);
        List<Developer> newDevelopers = new ArrayList<>();
        gameDTO.getDevelopers().forEach(id -> newDevelopers.add(developerService.findDeveloperById(id)));
        List<Platform> newPlatforms= new ArrayList<>();
        gameDTO.getPlatforms().forEach(id -> newPlatforms.add(platformService.findPlatformById(id)));
        logger.debug(gameDTO.getName() + " developed by: " + newDevelopers);
        logger.debug("Genres: " + gameDTO.getGenres());
        Game newGame = new Game(gameDTO.getName(),gameDTO.getRating(), gameDTO.getReleaseDate(), gameDTO.getGenres(), newPlatforms);
        newGame.setDevelopers(newDevelopers);
        logger.debug("New game added: " + newGame);
        gameService.addGame(newGame);
        return "redirect:/games";
    }
}
