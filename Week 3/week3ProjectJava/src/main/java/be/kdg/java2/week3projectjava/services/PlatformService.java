package be.kdg.java2.week3projectjava.services;

import be.kdg.java2.week3projectjava.domain.Developer;
import be.kdg.java2.week3projectjava.domain.Game;
import be.kdg.java2.week3projectjava.domain.GameGenres;
import be.kdg.java2.week3projectjava.domain.Platform;

import java.time.LocalDate;
import java.util.List;

public interface PlatformService {
    void savePlatformsToJson(List<Platform> platforms);
    List<Platform> showAllPlatforms();
    Platform addPlatform(Platform platform);
    Platform findPlatformById(int id);
}
