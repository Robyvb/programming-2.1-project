package be.kdg.java2.week10ProjectJava.domain;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "games")
public class Game extends EntityDomain{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "game_name")
    private String name;
    @Column(name = "game_rating")
    private double rating;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "game_release_date")
    private LocalDate releaseDate;

    @ElementCollection(fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    private List<Genres> genres;

    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name="platform_game",
            joinColumns = @JoinColumn(name= "game_id"),
            inverseJoinColumns = @JoinColumn(name="platform_id"))
    //TODO: any other way to implement?
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Platform> platforms;

    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name="game_developer",
            joinColumns = @JoinColumn(name="game_id"),
            inverseJoinColumns = @JoinColumn(name="developer_id"))
    //TODO: any other way to implement?
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Developer> developers;

    public Game(String name, double rating, LocalDate releaseDate, List<Genres> genres) {
        this.name = name;
        this.rating = rating;
        this.releaseDate = releaseDate;
        this.genres = genres;
        this.platforms = new ArrayList<>();
        this.developers = new ArrayList<>();
    }

    public Game(String name, double rating, LocalDate releaseDate, List<Genres> genres, List<Platform> platforms, List<Developer> developers) {
        this.name = name;
        this.rating = rating;
        this.releaseDate = releaseDate;
        this.genres = genres;
        this.platforms = platforms;
        this.developers = developers;
    }

    protected Game(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Developer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<Developer> developers) {
        this.developers = developers;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public List<Genres> getGenres() {
        return genres;
    }

    public void setGenres(List<Genres> genres) {
        this.genres = genres;
    }

    public List<Platform> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<Platform> platforms) {
        this.platforms = platforms;
    }

    public void addDeveloper(Developer developer){
        if(this.developers == null){
            this.developers = new ArrayList<>();
        }
        this.developers.add(developer);
    }

    public List<String> returnDeveloperNames(){
        List<String> developerNames = new ArrayList<>();
        developers.stream().forEach(developer -> developerNames.add(developer.toString()));
        return developerNames;
    }

    @Override
    public String toString() {
        return name;
    }
}
