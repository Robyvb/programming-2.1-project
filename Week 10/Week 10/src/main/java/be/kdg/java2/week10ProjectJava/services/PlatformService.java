package be.kdg.java2.week10ProjectJava.services;

import be.kdg.java2.week10ProjectJava.domain.Platform;

import java.util.List;

public interface PlatformService {
    void savePlatformsToJson(List<Platform> platforms);
    List<Platform> showAllPlatforms();
    Platform addPlatform(Platform platform);
    Platform findPlatformById(int id);
}
