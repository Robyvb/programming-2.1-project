package be.kdg.java2.week10ProjectJava.presentation.dtos;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class FilterGameDTO {
    private double rating;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate releaseDate;

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }
}
