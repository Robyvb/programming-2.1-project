package be.kdg.java2.week10ProjectJava.converters;


import be.kdg.java2.week10ProjectJava.domain.Genres;
import org.springframework.core.convert.converter.Converter;

public class StringToGameGenreConverter implements Converter<String, Genres> {
    @Override
    public Genres convert(String source) {
        if(source.toLowerCase().startsWith("act")) return Genres.ACTION;
        if(source.toLowerCase().startsWith("adv")) return Genres.ADVENTURE;
        if(source.toLowerCase().startsWith("rpg")) return Genres.RPG;
        if(source.toLowerCase().startsWith("fig")) return Genres.FIGHTING;
        if(source.toLowerCase().startsWith("sho")) return Genres.SHOOTER;
        if(source.toLowerCase().startsWith("fp")) return Genres.FIRST_PERSON;

        return null;
    }
}
