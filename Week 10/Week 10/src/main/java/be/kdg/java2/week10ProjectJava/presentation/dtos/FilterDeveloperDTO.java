package be.kdg.java2.week10ProjectJava.presentation.dtos;

public class FilterDeveloperDTO {
    private String firstName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
