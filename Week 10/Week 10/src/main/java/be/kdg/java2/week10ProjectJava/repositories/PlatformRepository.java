package be.kdg.java2.week10ProjectJava.repositories;

import be.kdg.java2.week10ProjectJava.domain.Platform;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlatformRepository extends JpaRepository<Platform, Integer> {
}
