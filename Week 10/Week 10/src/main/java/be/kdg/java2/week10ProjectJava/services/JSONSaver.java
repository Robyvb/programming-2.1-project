package be.kdg.java2.week10ProjectJava.services;

import be.kdg.java2.week10ProjectJava.domain.Developer;
import be.kdg.java2.week10ProjectJava.domain.Game;
import be.kdg.java2.week10ProjectJava.domain.Platform;

import java.util.List;

public interface JSONSaver {
    void saveGamesToJson(List<Game> games);
    void saveDevsToJson(List<Developer> developers);
    void savePlatformsToJson(List<Platform> platforms);
}
