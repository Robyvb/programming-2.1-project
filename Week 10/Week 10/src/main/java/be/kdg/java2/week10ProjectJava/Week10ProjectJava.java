package be.kdg.java2.week10ProjectJava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week10ProjectJava {
    public static void main(String[] args) {
        SpringApplication.run(Week10ProjectJava.class, args);
    }
}
