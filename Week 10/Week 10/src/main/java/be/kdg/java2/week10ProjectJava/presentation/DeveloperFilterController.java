package be.kdg.java2.week10ProjectJava.presentation;

import be.kdg.java2.week10ProjectJava.domain.Developer;
import be.kdg.java2.week10ProjectJava.presentation.dtos.FilterDeveloperDTO;
import be.kdg.java2.week10ProjectJava.services.DeveloperService;
import be.kdg.java2.week10ProjectJava.services.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/filterDevelopers")
public class DeveloperFilterController {
    private final Logger logger = LoggerFactory.getLogger(DeveloperFilterController.class);
    private final DeveloperService developerService;

    public DeveloperFilterController(DeveloperService developerService) {
        logger.debug("Creating developerFilterController");
        this.developerService = developerService;
    }

    @GetMapping
    public String filterDevelopers(Model model){
        logger.debug("Running filter developers page");
        //add DTO for th:object --> let form write to object
        model.addAttribute("filterDeveloperDTO", new FilterDeveloperDTO());
        return "filterDevelopers";
    }

    @PostMapping
    public String processFilterDevelopers(Model model, @Valid @ModelAttribute("filterDeveloperDTO") FilterDeveloperDTO filterDeveloperDTO) {
        //return filtered developers to page
        logger.debug("Filtering developer by first name: " + filterDeveloperDTO.getFirstName());
        List<Developer> foundDevelopers = developerService.findByFirstName(filterDeveloperDTO.getFirstName());
        logger.debug("Found developers: " + foundDevelopers);
        model.addAttribute("foundDevelopers", foundDevelopers);
        return "filterDevelopers";
    }
}
