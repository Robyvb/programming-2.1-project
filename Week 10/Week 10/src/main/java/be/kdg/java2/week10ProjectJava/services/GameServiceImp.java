package be.kdg.java2.week10ProjectJava.services;

import be.kdg.java2.week10ProjectJava.domain.Game;
import be.kdg.java2.week10ProjectJava.repositories.GameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.List;

@Service
public class GameServiceImp implements GameService {
    private GameRepository repository;
    private JSONSaver jsonSaver;
    private static final Logger logger = LoggerFactory.getLogger(GameServiceImp.class);
    @Autowired
    //JSON saving goes here too
    public GameServiceImp(GameRepository repository){
        this.repository = repository;
    }

    @Override
    public void saveGamesToJson(List<Game> games){
        jsonSaver.saveGamesToJson(games);
    }

    @Override
    public List<Game> showAllGames() {
        return repository.findAll();
    }

    @Override
    public Game addGame(Game game) {
        logger.debug("Adding game:" + game.getName());
        return repository.save(game);
    }

    @Override
    public Game findGameById(int id){
        logger.debug("Finding game by id " + id);
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException("error"));
    }

    @Override
    public List<Game> addAllGames(List<Game> games) {
        return repository.saveAll(games);
    }

    @Override
    public List<Game> findByRatingGreaterThanEqual(double rating) {
        return repository.findByRatingGreaterThanEqual(rating);
    }

    @Override
    public List<Game> findByRatingAndReleaseDateGreaterThanEqual(double rating, LocalDate releaseDate) {
        //only one genre --> converter on text input
        return repository.findByRatingAndReleaseDateGreaterThanEqual(rating, releaseDate);
    }
}
