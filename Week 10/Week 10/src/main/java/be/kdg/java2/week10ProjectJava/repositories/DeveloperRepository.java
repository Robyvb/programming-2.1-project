package be.kdg.java2.week10ProjectJava.repositories;

import be.kdg.java2.week10ProjectJava.domain.Developer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeveloperRepository extends JpaRepository<Developer, Integer> {
    List<Developer> findByFirstName(String firstName);
}
