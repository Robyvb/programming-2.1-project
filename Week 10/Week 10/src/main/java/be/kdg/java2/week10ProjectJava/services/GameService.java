package be.kdg.java2.week10ProjectJava.services;

import be.kdg.java2.week10ProjectJava.domain.Game;

import java.time.LocalDate;
import java.util.List;

public interface GameService {
    void saveGamesToJson(List<Game> games);
    List<Game> showAllGames();
    Game addGame(Game game);
    Game findGameById(int id);
    List<Game> addAllGames(List<Game> games);
    List<Game> findByRatingGreaterThanEqual(double rating);
    List<Game> findByRatingAndReleaseDateGreaterThanEqual(double rating, LocalDate releaseDate);
}
