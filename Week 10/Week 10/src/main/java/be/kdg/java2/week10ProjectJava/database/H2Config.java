package be.kdg.java2.week10ProjectJava.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class H2Config {
    private static final Logger log = LoggerFactory.getLogger(H2Config.class);

    @Bean
    public DataSource dataSource(){
        log.debug("Connected to H2 Database");
        DataSource dataSource = DataSourceBuilder.create()
                .driverClassName("org.h2.Driver")
                .url("jdbc:h2:mem:h2DBProject")
                .username("sa")
                .password("")
                .build();
        return dataSource;
    }
}
