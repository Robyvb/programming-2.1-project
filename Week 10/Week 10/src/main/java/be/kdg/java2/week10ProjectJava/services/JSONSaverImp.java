package be.kdg.java2.week10ProjectJava.services;

import be.kdg.java2.week10ProjectJava.domain.Developer;
import be.kdg.java2.week10ProjectJava.domain.Game;
import be.kdg.java2.week10ProjectJava.domain.Platform;
import be.kdg.java2.week10ProjectJava.utils.JsonDateTimeSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Repository;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Logger;

@Repository
public class JSONSaverImp implements JSONSaver {
    private final static Logger LOGGER = Logger.getLogger(JSONSaverImp.class.getName());
    private Gson gson;

    public JSONSaverImp() {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        builder.registerTypeAdapter(LocalDate.class, new JsonDateTimeSerializer());
        gson = builder.create();
    }

    @Override
    public void saveGamesToJson(List<Game> games) {
        LOGGER.info("Saving to games.json");
        try (FileWriter fileWriter = new FileWriter("games.json")) {
            fileWriter.write(gson.toJson(games));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveDevsToJson(List<Developer> developers) {
        LOGGER.info("Saving to developers.json");
        try (FileWriter fileWriter = new FileWriter("developers.json")) {
            fileWriter.write(gson.toJson(developers));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void savePlatformsToJson(List<Platform> platforms) {
        LOGGER.info("Saving to platforms.json");
        try (FileWriter fileWriter = new FileWriter("platforms.json")){
            fileWriter.write(gson.toJson(platforms));
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
