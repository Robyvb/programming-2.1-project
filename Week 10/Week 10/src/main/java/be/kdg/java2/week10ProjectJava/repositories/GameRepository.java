package be.kdg.java2.week10ProjectJava.repositories;

import be.kdg.java2.week10ProjectJava.domain.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface GameRepository extends JpaRepository<Game, Integer> {
    List<Game> findByRatingGreaterThanEqual(double rating);

    @Query("SELECT g FROM Game g WHERE g.rating >= ?1 AND g.releaseDate >= ?2")
    List<Game> findByRatingAndReleaseDateGreaterThanEqual(double rating, LocalDate releaseDate);
}
