package be.kdg.java2.week10ProjectJava.presentation;

import be.kdg.java2.week10ProjectJava.domain.Developer;
import be.kdg.java2.week10ProjectJava.domain.Game;
import be.kdg.java2.week10ProjectJava.presentation.dtos.DeveloperDTO;
import be.kdg.java2.week10ProjectJava.services.DeveloperService;
import be.kdg.java2.week10ProjectJava.services.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/developers")
public class DeveloperController{
    private final Logger logger = LoggerFactory.getLogger(DeveloperController.class);
    private final DeveloperService developerService;
    private final GameService gameService;

    public DeveloperController(DeveloperService developerService, GameService gameService){
        logger.debug("Creating developerController");
        this.gameService = gameService;
        this.developerService = developerService;
    }

    @GetMapping
    public String showAllDevelopers(Model model){
        logger.debug("Running showAllDevelopers");
        model.addAttribute("developers", developerService.showAllDevelopers());
        model.addAttribute("games", gameService.showAllGames());
        return "developers";
    }

    @GetMapping("/add")
    public String addDeveloper(Model model){
        logger.debug("User is adding a developer");
        model.addAttribute("games", gameService.showAllGames());
        model.addAttribute("developerDTO", new DeveloperDTO());
        return "addDeveloper";
    }

    @PostMapping("/add")
    public String processAddDeveloper(Model model, @Valid @ModelAttribute("developerDTO") DeveloperDTO developerDTO, BindingResult errors){
        if (errors.hasErrors()){
            logger.error("User forgot something while adding a developer");
            errors.getAllErrors().forEach(error -> logger.warn(error.toString()));

            //direct games back when error gets shown (else html page doesn't have the games anymore to show)
            model.addAttribute("games", gameService.showAllGames());
            return "addDeveloper";
        } else {
            logger.debug("DTO: " + developerDTO);

            //add new developer to the repository
            Developer newDeveloper = new Developer(developerDTO.getFirstName(), developerDTO.getSurname(), developerDTO.getBirthDate());
            //update every game that the new developer worked on to include them

            List<Game> newGames = new ArrayList<>();
            if(developerDTO.getGames() != null){
                developerDTO.getGames().forEach(id -> newGames.add(gameService.findGameById(id)));
                logger.debug("Adding developer to existing game");
                newGames.forEach(newGame -> newGame.addDeveloper(newDeveloper));
            }
            logger.debug("Games the developer worked on: " + newGames);
            logger.debug("Adding new developer to existing games");
            //is merging inside repository
            gameService.addAllGames(newGames);
            //newDeveloper.setGames(newGames);

            logger.debug("Developer: " + newDeveloper);
            developerService.addDeveloper(newDeveloper);
            return "redirect:/developers";
        }
    }

    @GetMapping("/detailsDeveloper")
    public String detailsDeveloper(@RequestParam("developerID") Integer developerID, Model model){
        //find the selected developer and show it
        Developer requestedDeveloper = developerService.findDeveloperById(developerID);
        model.addAttribute("developer", requestedDeveloper);
        return "detailsDeveloper";
    }
}
