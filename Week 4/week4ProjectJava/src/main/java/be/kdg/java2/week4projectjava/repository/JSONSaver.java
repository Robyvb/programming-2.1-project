package be.kdg.java2.week4projectjava.repository;

import be.kdg.java2.week4projectjava.domain.Developer;
import be.kdg.java2.week4projectjava.domain.Game;
import be.kdg.java2.week4projectjava.domain.Platform;

import java.util.List;

public interface JSONSaver {
    void saveGamesToJson(List<Game> games);
    void saveDevsToJson(List<Developer> developers);
    void savePlatformsToJson(List<Platform> platforms);
}
