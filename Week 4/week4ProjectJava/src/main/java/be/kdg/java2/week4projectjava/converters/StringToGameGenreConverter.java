package be.kdg.java2.week4projectjava.converters;


import be.kdg.java2.week4projectjava.domain.GameGenres;
import org.springframework.core.convert.converter.Converter;

public class StringToGameGenreConverter implements Converter<String, GameGenres> {
    @Override
    public GameGenres convert(String source) {
        /*
            ACTION("Action"),
            ADVENTURE("Adventure"),
            RPG("RPG"),
            FIGHTING("Fighting"),
            SHOOTER("Shooter"),
            FIRST_PERSON("First person");
        * */
        //source.toLowerCase().startsWith("..")


        if(source.toLowerCase().startsWith("act")) return GameGenres.ACTION;
        if(source.toLowerCase().startsWith("adv")) return GameGenres.ADVENTURE;
        if(source.toLowerCase().startsWith("rpg")) return GameGenres.RPG;
        if(source.toLowerCase().startsWith("fig")) return GameGenres.FIGHTING;
        if(source.toLowerCase().startsWith("sho")) return GameGenres.SHOOTER;
        if(source.toLowerCase().startsWith("fp")) return GameGenres.FIRST_PERSON;

        //default --> Should never reach!
        return null;
    }
}
