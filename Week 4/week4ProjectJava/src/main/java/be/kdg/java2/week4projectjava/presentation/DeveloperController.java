package be.kdg.java2.week4projectjava.presentation;

import be.kdg.java2.week4projectjava.domain.Developer;
import be.kdg.java2.week4projectjava.domain.Game;
import be.kdg.java2.week4projectjava.presentation.dtos.DeveloperDTO;
import be.kdg.java2.week4projectjava.services.DeveloperService;
import be.kdg.java2.week4projectjava.services.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/developers")
public class DeveloperController {
    private Logger logger = LoggerFactory.getLogger(DeveloperController.class);
    private DeveloperService developerService;
    private GameService gameService;

    public DeveloperController(DeveloperService developerService, GameService gameService){
        this.gameService = gameService;
        logger.debug("Creating developerController");
        this.developerService = developerService;
    }

    @GetMapping
    public String showAllDevelopers(Model model){
        logger.debug("Running showAllDevelopers");
        model.addAttribute("developers", developerService.showAllDevelopers());
        model.addAttribute("games", gameService.showAllGames());
        return "developers";
    }

    @GetMapping("/add")
    public String addDeveloper(Model model){
        logger.debug("User is adding a developer");
        model.addAttribute("games", gameService.showAllGames());
        return "addDeveloper";
    }

    @PostMapping("/add")
    public String processAddDeveloper(DeveloperDTO developerDTO){
        //logger.debug("Adding a developer: " + developerDTO.getFirstName() + "" + " with game ids: " + developerDTO.getGames());
        logger.debug("DTO: " + developerDTO);
        List<Game> newGames = new ArrayList<>();
        developerDTO.getGames().forEach(id -> newGames.add(gameService.findGameById(id)));
        logger.debug("Games the developer worked on: " + newGames);
        Developer testDeveloper = new Developer(developerDTO.getFirstName(), developerDTO.getSurname(), developerDTO.getBirthDate());
        testDeveloper.setGames(newGames);
        logger.debug("Developer: " + testDeveloper);
        developerService.addDeveloper(testDeveloper);
        return "redirect:/developers";
    }

    @GetMapping("/detailsDeveloper")
    public String detailsDeveloper(@RequestParam("developerID") Integer developerID, Model model){
        Developer requestedDeveloper = developerService.findDeveloperById(developerID);
        model.addAttribute("developer", requestedDeveloper);
        return "detailsDeveloper";
    }
}
