package be.kdg.java2.week4projectjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week4ProjectJavaApplication {
    public static void main(String[] args) {
        SpringApplication.run(Week4ProjectJavaApplication.class, args);
    }
}
