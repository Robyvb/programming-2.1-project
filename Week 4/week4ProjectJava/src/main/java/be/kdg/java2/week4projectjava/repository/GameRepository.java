package be.kdg.java2.week4projectjava.repository;

import be.kdg.java2.week4projectjava.domain.Game;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class GameRepository extends ListRepository<Game>{
    private Logger logger = LoggerFactory.getLogger(GameRepository.class);

    public GameRepository() {
        logger.debug("Creating game repository");
    }
}
