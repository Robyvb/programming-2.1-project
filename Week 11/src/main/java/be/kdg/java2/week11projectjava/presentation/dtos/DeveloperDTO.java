package be.kdg.java2.week11projectjava.presentation.dtos;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

public class DeveloperDTO {

    @NotEmpty(message = "Please fill in the first name")
    @Size(max = 70, message = "First name has a maximum length of 70")
    private String firstName;

    @NotEmpty(message = "Please fill in the surname")
    @Size(max = 70, message = "Surname has a maximum length of 70")
    private String surname;

    @NotNull(message = "Please fill in the birth date")
    @Past(message = "The birthdate must be in the past")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;

    //can be empty
    private List<Integer> games;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public List<Integer> getGames() {
        return games;
    }

    public void setGames(List<Integer> games) {
        this.games = games;
    }

    @Override
    public String toString() {
        return "DeveloperDTO{" +
                "firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + birthDate +
                ", games=" + games +
                '}';
    }
}
