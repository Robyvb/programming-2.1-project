package be.kdg.java2.week11projectjava.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Given rating is higher than 10")
public class RatingHigherThanTenException extends RuntimeException{
    public RatingHigherThanTenException(String message){
        super(message);
    }
}
