package be.kdg.java2.week11projectjava.domain;

public enum Genres {
    ACTION("Action"),
    ADVENTURE("Adventure"),
    RPG("RPG"),
    FIGHTING("Fighting"),
    SHOOTER("Shooter"),
    FIRST_PERSON("First person");

    private final String name;

    Genres(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
