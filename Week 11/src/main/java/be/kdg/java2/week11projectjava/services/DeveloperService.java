package be.kdg.java2.week11projectjava.services;

import be.kdg.java2.week11projectjava.domain.Developer;
import be.kdg.java2.week11projectjava.presentation.dtos.DeveloperDTO;

import java.util.List;

public interface DeveloperService {
    void saveDevelopersToJson(List<Developer> developers);
    List<Developer> showAllDevelopers();
    Developer addDeveloper(Developer developer);
    Developer findDeveloperById(int id);
    List<Developer> showAllDeveloperGames();
    List<Developer> findByFirstName(String firstName);
}
