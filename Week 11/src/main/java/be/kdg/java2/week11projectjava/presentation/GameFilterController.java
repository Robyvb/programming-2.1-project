package be.kdg.java2.week11projectjava.presentation;

import be.kdg.java2.week11projectjava.domain.Game;
import be.kdg.java2.week11projectjava.presentation.dtos.FilterGameDTO;
import be.kdg.java2.week11projectjava.services.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/filterGames")
public class GameFilterController {
    private final Logger logger = LoggerFactory.getLogger(DeveloperFilterController.class);
    private final GameService gameService;

    public GameFilterController(GameService gameService) {
        logger.debug("Creating game filter controller");
        this.gameService = gameService;
    }

    @GetMapping
    public String filterGames(Model model){
        model.addAttribute("filterGameDTO", new FilterGameDTO());
        return "filterGames";
    }

    @PostMapping
    public String processFilterGames(Model model, @Valid @ModelAttribute("filterGameDTO") FilterGameDTO filterGameDTO){
        logger.debug("Filtering game by rating higher than: " + filterGameDTO.getRating());

        List<Game> foundGames = new ArrayList<>();
        if(filterGameDTO.getReleaseDate() == null){
            //if input field hasn't been given anything
            foundGames.addAll(gameService.findByRatingGreaterThanEqual(filterGameDTO.getRating()));
        }else{
            foundGames.addAll(gameService.findByRatingAndReleaseDateGreaterThanEqual(filterGameDTO.getRating(), filterGameDTO.getReleaseDate()));
        }

        logger.debug("Found games: " + foundGames);
        model.addAttribute("foundGames", foundGames);
        return "filterGames";
    }
}