package be.kdg.java2.week11projectjava.repositories;

import be.kdg.java2.week11projectjava.domain.Developer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeveloperRepository extends JpaRepository<Developer, Integer> {
    List<Developer> findByFirstName(String firstName);
}
