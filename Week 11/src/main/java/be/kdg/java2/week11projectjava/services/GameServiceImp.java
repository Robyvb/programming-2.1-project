package be.kdg.java2.week11projectjava.services;

import be.kdg.java2.week11projectjava.domain.Game;
import be.kdg.java2.week11projectjava.exceptions.DateInFutureException;
import be.kdg.java2.week11projectjava.exceptions.RatingHigherThanTenException;
import be.kdg.java2.week11projectjava.repositories.GameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.List;

@Service
public class GameServiceImp implements GameService {
    private GameRepository repository;
    private JSONSaver jsonSaver;
    private static final Logger logger = LoggerFactory.getLogger(GameServiceImp.class);
    @Autowired
    //JSON saving goes here too
    public GameServiceImp(GameRepository repository){
        this.repository = repository;
    }

    @Override
    public void saveGamesToJson(List<Game> games){
        jsonSaver.saveGamesToJson(games);
    }

    @Override
    public List<Game> showAllGames() {
        return repository.findAll();
    }

    @Override
    public Game addGame(Game game) {
        logger.debug("Adding game:" + game.getName());
        return repository.save(game);
    }

    @Override
    public Game findGameById(int id){
        logger.debug("Finding game by id " + id);
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException("error"));
    }

    @Override
    public List<Game> addAllGames(List<Game> games) {
        return repository.saveAll(games);
    }

    @Override
    public List<Game> findByRatingGreaterThanEqual(double rating) {
        logger.debug("User is filtering on games by rating higher than or equal to given ");
        if(rating > 10){
            logger.error("User has generated a RatingHigherThanTenException");
            throw new RatingHigherThanTenException("The given rating is higher than 10");
        }
        return repository.findByRatingGreaterThanEqual(rating);
    }

    @Override
    public List<Game> findByRatingAndReleaseDateGreaterThanEqual(double rating, LocalDate releaseDate) {
        //only one genre --> converter on text input
        logger.debug("User is filtering on games by rating and release date higher than or equal to given");
        //check if the given date to filter on is in the future
        if(releaseDate.isAfter(LocalDate.now())){
            logger.error("User has generated a DateInFutureException");
            throw new DateInFutureException("The given date is in the future");
        }
        return repository.findByRatingAndReleaseDateGreaterThanEqual(rating, releaseDate);
    }
}
