package be.kdg.java2.week11projectjava.repositories;

import be.kdg.java2.week11projectjava.domain.Platform;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlatformRepository extends JpaRepository<Platform, Integer> {
}
