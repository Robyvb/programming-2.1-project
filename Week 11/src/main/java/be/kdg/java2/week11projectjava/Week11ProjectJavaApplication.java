package be.kdg.java2.week11projectjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week11ProjectJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(Week11ProjectJavaApplication.class, args);
    }

}
