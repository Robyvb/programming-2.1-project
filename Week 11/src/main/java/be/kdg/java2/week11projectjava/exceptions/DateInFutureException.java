package be.kdg.java2.week11projectjava.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Given date is in the future")
public class DateInFutureException extends RuntimeException{
    public DateInFutureException(String message){
        super(message);
    }
}
