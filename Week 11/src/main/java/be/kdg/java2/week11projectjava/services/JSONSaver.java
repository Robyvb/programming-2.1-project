package be.kdg.java2.week11projectjava.services;

import be.kdg.java2.week11projectjava.domain.Developer;
import be.kdg.java2.week11projectjava.domain.Game;
import be.kdg.java2.week11projectjava.domain.Platform;

import java.util.List;

public interface JSONSaver {
    void saveGamesToJson(List<Game> games);
    void saveDevsToJson(List<Developer> developers);
    void savePlatformsToJson(List<Platform> platforms);
}
