package be.kdg.java2.week11projectjava.services;

import be.kdg.java2.week11projectjava.domain.Developer;
import be.kdg.java2.week11projectjava.domain.Game;
import be.kdg.java2.week11projectjava.presentation.dtos.DeveloperDTO;
import be.kdg.java2.week11projectjava.repositories.DeveloperRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
public class DeveloperServiceImp implements DeveloperService{
    private DeveloperRepository repository;
    private JSONSaver jsonSaver;
    private static final Logger logger = LoggerFactory.getLogger(DeveloperServiceImp.class);
    @Autowired
    public DeveloperServiceImp(DeveloperRepository repository){
        this.repository = repository;
    }

    public void saveDevelopersJson(List<Developer> developers){
        jsonSaver.saveDevsToJson(developers);
    }

    @Override
    public void saveDevelopersToJson(List<Developer> developers){
        jsonSaver.saveDevsToJson(developers);
    }

    @Override
    public List<Developer> showAllDevelopers() {
        return repository.findAll();
    }

    @Override
    public List<Developer> showAllDeveloperGames(){
        return repository.findAll();
    }

    @Override
    public List<Developer> findByFirstName(String firstName) {
        return repository.findByFirstName(firstName);
    }

    @Override
    public Developer addDeveloper(Developer developer) {
        logger.debug("Adding developer: " + developer.getFirstName());
        return repository.save(developer);
    }

    @Override
    public Developer findDeveloperById(int id){
        logger.debug("Finding developer by id " + id);
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException("error"));
    }
}
