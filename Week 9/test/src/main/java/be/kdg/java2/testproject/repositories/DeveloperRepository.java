package be.kdg.java2.testproject.repositories;

import be.kdg.java2.testproject.domain.Developer;

import java.util.List;

public interface DeveloperRepository {
    List<Developer> read();
    Developer findById(int id);
    Developer create(Developer developer);
    List<Developer> createAll(List<Developer> developer);
}
