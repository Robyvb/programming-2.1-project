package be.kdg.java2.testproject.repositories;

import be.kdg.java2.testproject.domain.Developer;
import be.kdg.java2.testproject.domain.Game;
import be.kdg.java2.testproject.domain.Platform;

import java.util.List;

public interface JSONSaver {
    void saveGamesToJson(List<Game> games);
    void saveDevsToJson(List<Developer> developers);
    void savePlatformsToJson(List<Platform> platforms);
}
