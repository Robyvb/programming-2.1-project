package be.kdg.java2.testproject.repositories;

import be.kdg.java2.testproject.domain.Game;

import java.util.List;

public interface GameRepository {
    List<Game> read();
    Game findById(int id);
    Game create(Game game);
    List<Game> createAll(List<Game> games);
}
