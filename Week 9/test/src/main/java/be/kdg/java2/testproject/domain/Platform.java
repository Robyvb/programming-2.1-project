package be.kdg.java2.testproject.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "platforms")
public class Platform extends EntityDomain{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "platform_name")
    private String platformName;
    @Column(name = "platform_abbreviation")
    private String abbreviation;
    @Column(name = "platform_maintainer")
    private String platformMaintainer;
    @Column(name = "platform_version")
    private int version;

    @ManyToMany(mappedBy = "platforms", cascade = {CascadeType.DETACH, CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    private List<Game> games;

    public Platform(String platformName, String abbreviation, String platformMaintainer, int version) {
        this.platformName = platformName;
        this.abbreviation = abbreviation;
        this.platformMaintainer = platformMaintainer;
        this.version = version;
        //new platform doesn't have games initially - avoid null pointer exceptions
        this.games = new ArrayList<>();
    }

    protected Platform() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getPlatformMaintainer() {
        return platformMaintainer;
    }

    public void setPlatformMaintainer(String platformMaintainer) {
        this.platformMaintainer = platformMaintainer;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    @Override
    public String toString() {
        return platformName + "(" + abbreviation + ") " + version + " - " + platformMaintainer;
    }
}
