package be.kdg.java2.testproject.repositories;

import be.kdg.java2.testproject.domain.Developer;
import be.kdg.java2.testproject.domain.Platform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Repository
public class PlatformRepositoryImp implements PlatformRepository{
    private final Logger logger = LoggerFactory.getLogger(PlatformRepositoryImp.class);

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;
    private EntityManager em;

    public PlatformRepositoryImp() {
        logger.debug("Creating platform repository JPA");
    }

    @Override
    public List<Platform> read() {
        logger.debug("Reading platforms JPA");
        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List<Platform> platforms = em.createQuery("SELECT p FROM Platform p").getResultList();
        return platforms;
    }

    @Override
    public Platform findById(int id) {
        logger.debug("Finding platform by id JPA: " + id);
        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        Platform foundPlatform = em.find(Platform.class, id);
        em.getTransaction().commit();
        em.close();
        return foundPlatform;
    }

    @Override
    public Platform create(Platform platform) {
        logger.debug("Adding platform JPA: " + platform.toString());
        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.persist(platform);
        em.getTransaction().commit();
        em.close();
        return platform;
    }

    @Override
    public List<Platform> createAll(List<Platform> platforms) {
        platforms.forEach(this::create);
        return platforms;
    }
}
