package be.kdg.java2.testproject.services;

import be.kdg.java2.testproject.domain.Game;
import be.kdg.java2.testproject.repositories.GameRepository;
import be.kdg.java2.testproject.repositories.JSONSaver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GameServiceImp implements GameService {
    private GameRepository repository;
    private JSONSaver jsonSaver;
    private static final Logger logger = LoggerFactory.getLogger(GameServiceImp.class);
    @Autowired
    //JSON saving goes here too
    public GameServiceImp(GameRepository repository){
        this.repository = repository;
    }

    @Override
    public void saveGamesToJson(List<Game> games){
        jsonSaver.saveGamesToJson(games);
    }

    @Override
    public List<Game> showAllGames() {
        return repository.read();
    }

    @Override
    public Game addGame(Game game) {
        logger.debug("Adding game:" + game.getName());
        return repository.create(game);
    }

    @Override
    public Game findGameById(int id){
        logger.debug("Finding game by id " + id);
        return repository.findById(id);
    }

    @Override
    public List<Game> addAllGames(List<Game> games) {
        return repository.createAll(games);
    }
}
