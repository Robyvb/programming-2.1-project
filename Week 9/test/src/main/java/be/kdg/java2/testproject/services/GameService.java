package be.kdg.java2.testproject.services;

import be.kdg.java2.testproject.domain.Game;

import java.util.List;

public interface GameService {
    void saveGamesToJson(List<Game> games);
    List<Game> showAllGames();
    Game addGame(Game game);
    Game findGameById(int id);
    List<Game> addAllGames(List<Game> games);
}
