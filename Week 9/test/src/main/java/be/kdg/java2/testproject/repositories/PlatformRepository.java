package be.kdg.java2.testproject.repositories;

import be.kdg.java2.testproject.domain.Game;
import be.kdg.java2.testproject.domain.Platform;

import java.util.List;

public interface PlatformRepository {
    List<Platform> read();
    Platform findById(int id);
    Platform create(Platform platform);
    List<Platform> createAll(List<Platform> platforms);
}
