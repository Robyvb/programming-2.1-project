package be.kdg.java2.testproject.bootstrap;

import be.kdg.java2.testproject.domain.Developer;
import be.kdg.java2.testproject.domain.Game;
import be.kdg.java2.testproject.domain.Genres;
import be.kdg.java2.testproject.domain.Platform;
import be.kdg.java2.testproject.repositories.DeveloperRepository;
import be.kdg.java2.testproject.repositories.GameRepository;
import be.kdg.java2.testproject.repositories.PlatformRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Component
@Transactional
public class SeedData implements CommandLineRunner {
    private final Logger logger = LoggerFactory.getLogger(SeedData.class);

    private final DeveloperRepository developerRepository;
    private final GameRepository gameRepository;
    private final PlatformRepository platformRepository;


    public SeedData(DeveloperRepository developerRepository, GameRepository gameRepository, PlatformRepository platformRepository) {
        logger.debug("Creating repositories SeedData");
        this.developerRepository = developerRepository;
        this.gameRepository = gameRepository;
        this.platformRepository = platformRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        logger.debug("Running seed data");
//        Old code

//        Platform windows = new Platform("Windows","WIN","Microsoft",10);
//        Platform playstation = new Platform ("Playstation","PS","Sony",5);
//        Platform xbox = new Platform ("XBOX","XB","Microsoft",5);
//        Platform switchConsole = new Platform ("Switch", "SW","Nintendo",1);
//
//        Developer gabeNewell = new Developer("Gabe","Newell", LocalDate.of(1962,10,3));
//        Developer adamKicinski = new Developer("Adam","Kicinski", LocalDate.of(1969,4,21));
//        Developer marcinBlacha = new Developer("Marcin","Blacha", LocalDate.of(1972,11,10));
//        Developer marcMerril = new Developer("Marc","Merrill", LocalDate.of(1980,8,17));
//        Developer bobbyKotick = new Developer("Bobby","Kotick", LocalDate.of(1963,10,12));
//        Developer adamBoyes = new Developer("Adam","Boyes", LocalDate.of(2016,8,8));
//
//        // List<Developer> developers = developerRepo.saveAll(
//        //      List.of(
//        //              gabeNewell
//        //              adamKicinski
//        //              marcinBlacha
//        //              marcMerril
//        //              bobbyKotick
//        //              adamBoyes
//        //    )
//        // )
//
//        // better:
//        // List<Developer> developers = generateDevelopers();
//
////        Developer gabeNewell = developers.get(0);
////        Developer adamKicinski = developers.get(1);
////        Developer marcinBlacha = developers.get(2);
////        Developer marcMerril = developers.get(3);
////        Developer bobbyKotick = developers.get(4);
////        Developer adamBoyes = developers.get(5);
//
//        Game CSGO = new Game("Counter-Strike: Global Offensive", 9.5, LocalDate.of(2012,8,21),
//                List.of(Genres.ACTION, Genres.FIRST_PERSON, Genres.SHOOTER));
//        Game witcher3 = new Game("The Witcher 3", 9, LocalDate.of(2015,5,18),
//                List.of(Genres.ACTION, Genres.ADVENTURE, Genres.FIGHTING, Genres.RPG));
//        Game LOL = new Game("League Of Legends", 10, LocalDate.of(2009,10,27),
//                List.of(Genres.FIGHTING, Genres.RPG, Genres.ACTION));
//        Game OW = new Game("Overwatch", 8, LocalDate.of(2016,5,24),
//                List.of(Genres.ACTION, Genres.FIRST_PERSON, Genres.SHOOTER));
//
//        windows.setGames(List.of(CSGO, LOL, OW));
//        playstation.setGames(List.of(witcher3, OW));
//        xbox.setGames(List.of(witcher3,LOL));
//        switchConsole.setGames(List.of(OW));
//
//        CSGO.setPlatforms(List.of(windows));
//        CSGO.setDevelopers(List.of(gabeNewell,marcinBlacha));
////        CSGO.setDevelopers(List.of(developers.get(0), developers.get(2)));
//
//        witcher3.setPlatforms(List.of(playstation, xbox));
//        witcher3.setDevelopers(List.of(adamKicinski,marcinBlacha));
//
//        LOL.setPlatforms(List.of(windows, xbox));
//        LOL.setDevelopers(List.of(marcMerril,gabeNewell,bobbyKotick));
//
//        OW.setPlatforms(List.of(windows, playstation, xbox, switchConsole));
//        OW.setDevelopers(List.of(bobbyKotick, adamBoyes, adamKicinski));
//
//        /*
//        platformRepository.create(windows);
//        platformRepository.create(playstation);
//        platformRepository.create(xbox);
//        platformRepository.create(switchConsole);
//         */
//
//
        List<Platform> platforms = generatePlatforms();
        List<Developer> developers = generateDevelopers();
        List<Game> games = generateGames(platforms, developers);

        List<Developer> dbDevelopers = developerRepository.read();
        List<Game> dbGames = gameRepository.read();
        List<Platform> dbPlatforms = platformRepository.read();

        //testing logger output
//        logger.debug("Developer: " + dbDevelopers.get(0));
//        logger.debug("Developed games: " + dbDevelopers.get(0).getGames());
//        //logger.debug("Developed games: " + developerRepository.read().get(0).getGames());
//        logger.debug("Game: " + dbGames.get(0));
//        //logger.debug("Game: " + gameRepository.read().get(0));
//        logger.debug("Developed by: " + dbGames.get(0).getDevelopers());
//        //logger.debug("Developed by: " + gameRepository.read().get(0).getDevelopers());
//        logger.debug("Is on these platforms: " + dbGames.get(0).getPlatforms());
//        //logger.debug("Is on these platforms: " + gameRepository.read().get(0).getPlatforms());
//        logger.debug("Platform: " + dbPlatforms.get(0));
//        //logger.debug("Platform: " + platformRepository.read().get(0));
    }

     public List<Platform> generatePlatforms() {
        return platformRepository.createAll(
           List.of(
                   new Platform("Windows","WIN","Microsoft",10),
                   new Platform ("Playstation","PS","Sony",5),
                   new Platform ("XBOX","XB","Microsoft",5),
                   new Platform ("Switch", "SW","Nintendo",1)
           )
        );
     }

     public List<Developer> generateDevelopers() {
          return developerRepository.createAll(
               List.of(
                       new Developer("Gabe","Newell", LocalDate.of(1962,10,3)),
                       new Developer("Adam","Kicinski", LocalDate.of(1969,4,21)),
                       new Developer("Marcin","Blacha", LocalDate.of(1972,11,10)),
                       new Developer("Marc","Merrill", LocalDate.of(1980,8,17)),
                       new Developer("Bobby","Kotick", LocalDate.of(1963,10,12)),
                       new Developer("Adam","Boyes", LocalDate.of(2016,8,8))
             )
          );
     }

     public List<Game> generateGames(List<Platform> platforms, List<Developer> developers) {
        Developer gabeNewell = developers.get(0);
        Developer adamKicinski = developers.get(1);
        Developer marcinBlacha = developers.get(2);
        Developer marcMerril = developers.get(3);
        Developer bobbyKotick = developers.get(4);
        Developer adamBoyes = developers.get(5);

        Platform windows = platforms.get(0);
        Platform playstation = platforms.get(1);
        Platform xbox = platforms.get(2);
        Platform switchConsole = platforms.get(3);

        return gameRepository.createAll(
                List.of(
                        new Game("Counter-Strike: Global Offensive", 9.5, LocalDate.of(2012,8,21), List.of(Genres.ACTION, Genres.FIRST_PERSON, Genres.SHOOTER), List.of(windows, playstation), List.of(gabeNewell, adamKicinski)),
                        new Game("The Witcher 3", 9, LocalDate.of(2015,5,18), List.of(Genres.ACTION, Genres.ADVENTURE, Genres.FIGHTING, Genres.RPG), List.of(windows), List.of(adamKicinski, marcinBlacha)),
                        new Game("League Of Legends", 10, LocalDate.of(2009,10,27), List.of(Genres.FIGHTING, Genres.RPG, Genres.ACTION), List.of(playstation, xbox), List.of(bobbyKotick)),
                        new Game("Halo 3", 9.5, LocalDate.of(2012,8,21), List.of(Genres.ACTION, Genres.FIRST_PERSON, Genres.SHOOTER), List.of(switchConsole, playstation), List.of(adamBoyes, adamKicinski)),
                        new Game("Overwatch", 8, LocalDate.of(2016,5,24), List.of(Genres.ACTION, Genres.FIRST_PERSON, Genres.SHOOTER), List.of(windows, playstation, switchConsole), List.of(marcMerril, marcinBlacha))
             )
        );
    }
}
