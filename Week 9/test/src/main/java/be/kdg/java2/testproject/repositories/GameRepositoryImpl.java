package be.kdg.java2.testproject.repositories;

import be.kdg.java2.testproject.domain.Developer;
import be.kdg.java2.testproject.domain.Game;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Repository
public class GameRepositoryImpl implements GameRepository{
    private final Logger logger = LoggerFactory.getLogger(GameRepositoryImpl.class);

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;
    private EntityManager em;

    public GameRepositoryImpl() {
        logger.debug("Creating game repository with JPA");
    }

    @Override
    public List<Game> read() {
        logger.debug("Reading games with JPA");
        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List<Game> games = em.createQuery("SELECT g FROM Game g").getResultList();
        em.getTransaction().commit();
        em.close();
        return games;
    }

    @Override
    public Game findById(int id) {
        logger.debug("Looking from game with id JPA: " + id);
        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        Game foundGame = em.find(Game.class, id);
        em.getTransaction().commit();
        em.close();
        return foundGame;
    }

    @Override
    public Game create(Game game) {
        logger.debug("Adding game JPA: " + game);
        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.merge(game);
        em.getTransaction().commit();
        em.close();
        return game;
    }

    @Override
    public List<Game> createAll(List<Game> games) {
        games.forEach(this::create);
        return games;
    }
}
