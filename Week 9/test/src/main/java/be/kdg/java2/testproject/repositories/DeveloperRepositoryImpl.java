package be.kdg.java2.testproject.repositories;

import be.kdg.java2.testproject.domain.Developer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Repository
public class DeveloperRepositoryImpl implements DeveloperRepository{
    private final Logger logger = LoggerFactory.getLogger(DeveloperRepositoryImpl.class);

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;
    private EntityManager em;

    public DeveloperRepositoryImpl() {
        logger.debug("Creating Developer repository JPA");
    }

    @Override
    public List<Developer> read() {
        logger.debug("Reading developers with JPA");
        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List<Developer> developers = em.createQuery("SELECT d FROM Developer d").getResultList();
        em.getTransaction().commit();
        em.close();
        return developers;
    }

    @Override
    public Developer findById(int id) {
        logger.debug("Finding developer with id JPA: " + id);
        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        Developer foundDeveloper = em.find(Developer.class, id);
        em.getTransaction().commit();
        em.close();
        return foundDeveloper;
    }

    @Override
    public Developer create(Developer developer) {
        logger.debug("Adding developer JPA: " + developer);
        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.persist(developer);
        em.getTransaction().commit();
        em.close();
        return developer;
    }

    @Override
    public List<Developer> createAll(List<Developer> developers) {
        developers.forEach(this::create);
        return developers;
    }
}
