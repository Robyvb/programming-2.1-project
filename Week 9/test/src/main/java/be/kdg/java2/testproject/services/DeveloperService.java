package be.kdg.java2.testproject.services;

import be.kdg.java2.testproject.domain.Developer;

import java.util.List;

public interface DeveloperService {
    void saveDevelopersToJson(List<Developer> developers);
    List<Developer> showAllDevelopers();
    Developer addDeveloper(Developer developer);
    Developer findDeveloperById(int id);
    List<Developer> showAllDeveloperGames();
}
