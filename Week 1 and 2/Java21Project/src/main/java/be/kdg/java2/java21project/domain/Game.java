package be.kdg.java2.java21project.domain;

import java.time.LocalDate;
import java.util.List;

public class Game {
    private final String name;
    private final double rating;
    private final LocalDate releaseDate;
    private List<GameGenres> genres;

    transient private List<Platform> platforms;
    transient private List<Developer> developers;

    public String getName() {
        return name;
    }

    public double getRating() {
        return rating;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public List<Platform> getPlatforms() {
        return platforms;
    }

    public List<Developer> getDevelopers() {
        return developers;
    }

    public List<GameGenres> getGenres() {
        return genres;
    }

    public void addGenres(List<GameGenres> genres){
        this.genres.addAll(genres);
    }

    public void addDevelopers(List<Developer> developers){
        this.developers.addAll(developers);
    }

    @Override
    public String toString() {
        return name + " has the rating of: " + rating +
                ".\nIt was developed in " + releaseDate + " by " + developers +
                ".\nIt is supported on these platforms: " + platforms;
    }

    public Game(String name, double rating, LocalDate releaseDate, List<GameGenres> genres, List<Platform> platforms, List<Developer> developers) {
        this.name = name;
        this.rating = rating;
        this.releaseDate = releaseDate;
        this.genres = genres;
        this.platforms = platforms;
        this.developers = developers;
    }
}