package be.kdg.java2.java21project.repository;

import be.kdg.java2.java21project.domain.Developer;
import be.kdg.java2.java21project.domain.Game;

import java.util.List;

public interface JSONSaver {
    void saveGamesToJson(List<Game> games);
    void saveDevsToJson(List<Developer> developers);
}
