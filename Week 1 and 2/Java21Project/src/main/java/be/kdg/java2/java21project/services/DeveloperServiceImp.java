package be.kdg.java2.java21project.services;

import be.kdg.java2.java21project.domain.Developer;
import be.kdg.java2.java21project.repository.GameRepository;
import be.kdg.java2.java21project.repository.JSONSaver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class DeveloperServiceImp implements DeveloperService{
    private GameRepository repository;
    private JSONSaver jsonSaver;
    @Autowired
    //JSON saving goes here too
    public DeveloperServiceImp(GameRepository repository, JSONSaver jsonSaver){
        this.repository = repository;
        this.jsonSaver = jsonSaver;
    }

    @Override
    public void saveDevelopersToJson(List<Developer> developers){
        jsonSaver.saveDevsToJson(developers);
    }

    @Override
    public List<Developer> showAllDevelopers() {
        return repository.getDevelopers();
    }

    @Override
    public Developer addDeveloper(String firstName, String surname, LocalDate birthDate) {
        Developer developer = new Developer(firstName,surname,birthDate);
        return developer;
    }
}
