package be.kdg.java2.java21project.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Developer{
    private final String firstName;
    private final String surname;
    private final LocalDate birthDate;

    transient private List<Game> games = new ArrayList<>();

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public List<Game> getGames() {
        return games;
    }

    public void addGames(List<Game> games){
        this.games.addAll(games);
    }

    @Override
    public String toString() {
        return surname + " " + firstName + " (born " + birthDate + ")";
    }

    public Developer(String firstName, String surname, LocalDate birthDate) {
        this.firstName = firstName;
        this.surname = surname;
        this.birthDate = birthDate;
    }
}
