package be.kdg.java2.java21project.services;

import be.kdg.java2.java21project.domain.Developer;
import be.kdg.java2.java21project.domain.Game;
import be.kdg.java2.java21project.domain.GameGenres;
import be.kdg.java2.java21project.domain.Platform;

import java.time.LocalDate;
import java.util.List;

public interface GameService {
    void saveGamesToJson(List<Game> games);
    List<Game> showAllGames();
    Game addGame(String name, double rating, LocalDate releaseDate, List<GameGenres> genres, List<Platform> platforms, List<Developer> developers);
}
