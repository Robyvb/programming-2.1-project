package be.kdg.java2.java21project.services;

import be.kdg.java2.java21project.domain.Developer;
import be.kdg.java2.java21project.domain.Game;
import be.kdg.java2.java21project.domain.GameGenres;
import be.kdg.java2.java21project.domain.Platform;
import be.kdg.java2.java21project.repository.GameRepository;
import be.kdg.java2.java21project.repository.JSONSaver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class GameServiceImp implements GameService{
    private GameRepository repository;
    private JSONSaver jsonSaver;
    @Autowired
    //JSON saving goes here too
    public GameServiceImp(GameRepository repository, JSONSaver jsonSaver){
        this.repository = repository;
        this.jsonSaver = jsonSaver;
    }

    @Override
    public void saveGamesToJson(List<Game> games){
        jsonSaver.saveGamesToJson(games);
    }

    @Override
    public List<Game> showAllGames() {
        return repository.getGames();
    }

    @Override
    public Game addGame(String name, double rating, LocalDate releaseDate, List<GameGenres> genres, List<Platform> platforms, List<Developer> developers) {
        Game game = new Game(name,rating,releaseDate,genres, platforms, developers);
        return game;
    }

}
