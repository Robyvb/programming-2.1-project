package be.kdg.java2.java21project.repository;

import be.kdg.java2.java21project.domain.Developer;
import be.kdg.java2.java21project.domain.Game;
import be.kdg.java2.java21project.domain.GameGenres;
import be.kdg.java2.java21project.domain.Platform;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class HardCodedGameRepository implements GameRepository{
    private static List<Platform> platforms = new ArrayList<>();
    private static List<Developer> developers  = new ArrayList<>();
    private static List<Game> games = new ArrayList<>();

    public HardCodedGameRepository() {
        seed();
    }

    private void seed(){
        Platform windows = new Platform("Windows","WIN","Microsoft",10);
        Platform playstation = new Platform ("Playstation","PS","Sony",5);
        Platform xbox = new Platform ("XBOX","XB","Microsoft",5);
        Platform switchConsole = new Platform ("Switch", "SW","Nintendo",1);

        Developer gabeNewell = new Developer("Gabe","Newell", LocalDate.of(1962,10,3));
        Developer adamKicinski = new Developer("Adam","Kicinski", LocalDate.of(1969,4,21));
        Developer marcinBlacha = new Developer("Marcin","Blacha", LocalDate.of(1972,11,10));
        Developer marcMerril = new Developer("Marc","Merrill", LocalDate.of(1980,8,17));
        Developer bobbyKotick = new Developer("Bobby","Kotick", LocalDate.of(1963,10,12));
        Developer adamBoyes = new Developer("Adam","Boyes", LocalDate.of(2016,8,8));

        Game CSGO = new Game("Counter-Strike: Global Offensive", 9.5, LocalDate.of(2012,8,21),
                List.of(GameGenres.ACTION,GameGenres.FIRST_PERSON,GameGenres.SHOOTER), List.of(windows),
                List.of(gabeNewell, marcinBlacha));
        Game witcher3 = new Game("The Witcher 3", 9, LocalDate.of(2015,5,18),
                List.of(GameGenres.ACTION,GameGenres.ADVENTURE,GameGenres.FIGHTING,GameGenres.RPG), List.of(playstation, xbox),
                List.of(adamKicinski,marcinBlacha));
        Game LOL = new Game("League Of Legends", 10, LocalDate.of(2009,10,27),
                List.of(GameGenres.FIGHTING, GameGenres.RPG,GameGenres.ACTION), List.of(windows, xbox),
                List.of(marcMerril, gabeNewell, bobbyKotick));
        Game OW = new Game("Overwatch", 8, LocalDate.of(2016,5,24),
                List.of(GameGenres.ACTION,GameGenres.FIRST_PERSON,GameGenres.SHOOTER), List.of(windows, playstation, xbox, switchConsole),
                List.of(bobbyKotick, adamBoyes,adamKicinski));

        gabeNewell.addGames(List.of(CSGO, OW));
        adamKicinski.addGames(List.of(witcher3, OW));
        marcinBlacha.addGames(List.of(CSGO, witcher3));
        marcMerril.addGames(List.of(LOL));
        bobbyKotick.addGames(List.of(LOL, OW));
        adamBoyes.addGames(List.of(OW));

        platforms.addAll(List.of(windows, playstation, xbox, switchConsole));
        developers.addAll(List.of(gabeNewell, adamKicinski, marcinBlacha, marcMerril, bobbyKotick, adamBoyes));
        games.addAll(List.of(CSGO, witcher3, LOL, OW));

    }

    @Override
    public List<Platform> getPlatforms(){
        return platforms;
    }

    @Override
    public List<Developer> getDevelopers(){
        return developers;
    }

    @Override
    public List<Game> getGames(){
        return games;
    }
}
