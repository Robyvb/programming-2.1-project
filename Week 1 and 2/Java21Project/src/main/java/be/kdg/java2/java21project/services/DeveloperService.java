package be.kdg.java2.java21project.services;

import be.kdg.java2.java21project.domain.Developer;

import java.time.LocalDate;
import java.util.List;

public interface DeveloperService {
    void saveDevelopersToJson(List<Developer> developers);
    List<Developer> showAllDevelopers();
    Developer addDeveloper(String firstName, String surname, LocalDate birthDate);
}
