package be.kdg.java2.java21project.domain;

public enum GameGenres {
    ACTION,
    ADVENTURE,
    RPG,
    FIGHTING,
    SHOOTER,
    FIRST_PERSON,
}
