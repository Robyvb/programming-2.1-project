package be.kdg.java2.java21project;

import be.kdg.java2.java21project.presentation.View;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Java21ProjectApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Java21ProjectApplication.class, args);
        View view = context.getBean(View.class);
        view.menu();
        context.close();
    }

}
