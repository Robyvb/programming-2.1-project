package be.kdg.java2.java21project.presentation;

import be.kdg.java2.java21project.domain.Developer;
import be.kdg.java2.java21project.domain.Game;
import be.kdg.java2.java21project.domain.GameGenres;
import be.kdg.java2.java21project.repository.GameRepository;
import be.kdg.java2.java21project.repository.JSONSaver;
import be.kdg.java2.java21project.services.DeveloperService;
import be.kdg.java2.java21project.services.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Component
public class View {
    private final DeveloperService developerService;
    private final GameService gameService;
    private final Scanner scanner = new Scanner(System.in);

    @Autowired
    public View(DeveloperService developerService, GameService gameService) {
        this.developerService = developerService;
        this.gameService = gameService;
    }

    public void menu(){
        int userChoice = 0;
        do{
            System.out.println("What would you like to do?");
            System.out.println("==========================");
            System.out.println("0) Quit");
            System.out.println("1) Show all games");
            System.out.println("2) Show all games of a certain genre");
            System.out.println("3) Show all developers");
            System.out.println("4) Show all developers with name and/or date of birth");
            System.out.print("Choice (0-4): ");
            userChoice = scanner.nextInt();
            switch(userChoice){
                case 0:
                    System.out.println("Shutting down...");
                    //skip to end
                    break;
                case 1:
                    //Show all games
                    System.out.println("Showing all games\n");
                    printAllGames();
                    break;
                case 2:
                    //Show all games of a certain genre
                    printCertainGameOnGenre();
                    break;
                case 3:
                    //Show all developers
                    System.out.println("Showing all developers\n");
                    printAllDevelopers();
                    break;
                case 4:
                    //Filter developers by name and DOB (functions are separated)
                    filterDevelopersByName();
                    break;
                default:
                    System.out.println("Please enter a number between 0 to 4.");
            }
        }while(userChoice != 0);
    }

    private void printAllGames(){
        gameService.showAllGames().forEach(game -> {
            System.out.println(game + "\n==========================");
        });
        gameService.saveGamesToJson(gameService.showAllGames());
        //new line
        System.out.println();
    }

    private void printAllDevelopers(){
        developerService.showAllDevelopers().forEach(System.out::println);
        developerService.saveDevelopersToJson(developerService.showAllDevelopers());
        //new line
        System.out.println();
    }

    private void printCertainGameOnGenre(){
        //TODO: Improve this (streams?)
        List<Game> filteredGames = new ArrayList<>();
        System.out.println("What genre would you like to see?");
        System.out.println("1: Action\n2: Adventure\n3: RPG\n4: Fighting\n5: Shooter\n6: First Person");
        //lazy fix
        switch(scanner.nextInt()){
            case 1:
                filteredGames = filterGames(GameGenres.ACTION);
                break;
            case 2:
                filteredGames = filterGames(GameGenres.ADVENTURE);
                break;
            case 3:
                filteredGames = filterGames(GameGenres.RPG);
                break;
            case 4:
                filteredGames = filterGames(GameGenres.FIGHTING);
                break;
            case 5:
                filteredGames = filterGames(GameGenres.SHOOTER);
                 break;
            case 6:
                filteredGames = filterGames(GameGenres.FIRST_PERSON);
                break;
            default:
                System.out.println("Please fill in a number between 1 and 6");
        }
        //Shouldn't appear
        if(filteredGames.isEmpty()){
            System.out.println("No games from this genre found..");
        }else{
            filteredGames.forEach(game -> System.out.println(game + "\n=========================="));
            gameService.saveGamesToJson(filteredGames);
        }
        //new line
        System.out.println();
    }

    private List<Game> filterGames(GameGenres selectedGenre){
        List<Game> filteredGames = new ArrayList<>();
        //Go through each genre in each game to locate the selected genre
        gameService.showAllGames().forEach(game -> game.getGenres().forEach(genre -> {
            if(genre.equals(selectedGenre)){
                filteredGames.add(game);
            }
        }));
        return filteredGames;
    }

    private void filterDevelopersByName(){
        //Filter vars
        String name;
        List<Developer> filteredDevelopers = new ArrayList<>();

        System.out.println("Enter (part of) a name or leave blank (not case sensitive)");
        //Empty buffer
        scanner.nextLine();
        //Convert to lower case
        name = scanner.nextLine().toLowerCase();

        if(name.isEmpty()){
            //All developers as parameter if name is left blank
            filterOnDOB(developerService.showAllDevelopers());
        }else{
            developerService.showAllDevelopers().forEach(developer -> {
                //First name and surname are separate -> check both
                //Also convert to lower case as well
                if(developer.getFirstName().toLowerCase().contains(name) || developer.getSurname().toLowerCase().contains(name)){
                    filteredDevelopers.add(developer);
                }
            });
            filterOnDOB(filteredDevelopers);
        }
    }

    private void filterOnDOB(List<Developer> developers){
        //Filter vars
        String stringDate;
        LocalDate date;
        List<Developer> filteredDevelopers = new ArrayList<>();

        System.out.println("Enter the date of birth (yyyy-mm-dd, ex. 1962-10-03) or leave blank");
        stringDate = scanner.nextLine();

        //Check if it's blank, if yes then don't filter
        if(stringDate.isEmpty()){
            //Print the given list of developers, no filter
            developers.forEach(System.out::println);
            developerService.saveDevelopersToJson(developers);
            System.out.println();
        }else{
            date = LocalDate.parse(stringDate);
            //Filter list
            developers.forEach(developer -> {
               if(developer.getBirthDate().equals(date)){
                   filteredDevelopers.add(developer);
               }
            });
            //Give message if nothing is found
            if(filteredDevelopers.isEmpty()){
                System.out.println("No developers found within these parameters..\n");
            }else{
                filteredDevelopers.forEach(System.out::println);
                //Save to developers.json
                developerService.saveDevelopersToJson(filteredDevelopers);
                //new line
                System.out.println();
            }
        }
    }
}
