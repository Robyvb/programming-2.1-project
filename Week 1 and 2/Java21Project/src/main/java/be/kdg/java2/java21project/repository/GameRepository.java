package be.kdg.java2.java21project.repository;

import be.kdg.java2.java21project.domain.Developer;
import be.kdg.java2.java21project.domain.Game;
import be.kdg.java2.java21project.domain.Platform;

import java.util.List;

public interface GameRepository {
    List<Platform> getPlatforms();
    List<Developer> getDevelopers();
    List<Game> getGames();
}
