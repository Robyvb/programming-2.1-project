package be.kdg.java2.java21project.domain;

import java.util.ArrayList;
import java.util.List;

public class Platform {
    private final String platformName;
    private final String abbreviation;
    private String platformMaintainer;
    private int version;

    transient private List<Game> games = new ArrayList<>();

    public String getPlatformName() {
        return platformName;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public String getPlatformMaintainer() {
        return platformMaintainer;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setPlatformMaintainer(String platformMaintainer) {
        this.platformMaintainer = platformMaintainer;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void addGames(List<Game> games){
        this.games.addAll(games);
    }

    @Override
    public String toString() {
        return platformName + "(" + abbreviation + ") is maintained by "
                + platformMaintainer + " and its version is: " + version;
    }

    public Platform(String platformName, String abbreviation, String platformMaintainer, int version) {
        this.platformName = platformName;
        this.abbreviation = abbreviation;
        this.platformMaintainer = platformMaintainer;
        this.version = version;
    }
}
