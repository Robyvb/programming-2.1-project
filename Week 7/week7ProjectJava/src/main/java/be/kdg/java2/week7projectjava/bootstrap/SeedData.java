package be.kdg.java2.week7projectjava.bootstrap;

import be.kdg.java2.week7projectjava.domain.Developer;
import be.kdg.java2.week7projectjava.domain.Game;
import be.kdg.java2.week7projectjava.domain.GameGenres;
import be.kdg.java2.week7projectjava.domain.Platform;
import be.kdg.java2.week7projectjava.repository.DeveloperRepositoryImpl;
import be.kdg.java2.week7projectjava.repository.GameRepositoryImpl;
import be.kdg.java2.week7projectjava.repository.PlatformRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Component
@Profile("collections")
public class SeedData implements CommandLineRunner {
    private Logger logger = LoggerFactory.getLogger(SeedData.class);
    private PlatformRepository platformRepository;
    private DeveloperRepositoryImpl developerRepositoryImpl;
    private GameRepositoryImpl gameRepositoryImpl;

    public SeedData(PlatformRepository platformRepository, DeveloperRepositoryImpl developerRepositoryImpl, GameRepositoryImpl gameRepositoryImpl) {
        this.platformRepository = platformRepository;
        this.developerRepositoryImpl = developerRepositoryImpl;
        this.gameRepositoryImpl = gameRepositoryImpl;
    }

    @Override
    public void run(String... args) throws Exception {
        Platform windows = new Platform("Windows","WIN","Microsoft",10);
        Platform playstation = new Platform ("Playstation","PS","Sony",5);
        Platform xbox = new Platform ("XBOX","XB","Microsoft",5);
        Platform switchConsole = new Platform ("Switch", "SW","Nintendo",1);

        Developer gabeNewell = new Developer("Gabe","Newell", LocalDate.of(1962,10,3));
        Developer adamKicinski = new Developer("Adam","Kicinski", LocalDate.of(1969,4,21));
        Developer marcinBlacha = new Developer("Marcin","Blacha", LocalDate.of(1972,11,10));
        Developer marcMerril = new Developer("Marc","Merrill", LocalDate.of(1980,8,17));
        Developer bobbyKotick = new Developer("Bobby","Kotick", LocalDate.of(1963,10,12));
        Developer adamBoyes = new Developer("Adam","Boyes", LocalDate.of(2016,8,8));

        Game CSGO = new Game("Counter-Strike: Global Offensive", 9.5, LocalDate.of(2012,8,21),
                List.of(GameGenres.ACTION,GameGenres.FIRST_PERSON,GameGenres.SHOOTER), List.of(windows),
                List.of(gabeNewell, marcinBlacha));
        Game witcher3 = new Game("The Witcher 3", 9, LocalDate.of(2015,5,18),
                List.of(GameGenres.ACTION,GameGenres.ADVENTURE,GameGenres.FIGHTING,GameGenres.RPG), List.of(playstation, xbox),
                List.of(adamKicinski,marcinBlacha));
        Game LOL = new Game("League Of Legends", 10, LocalDate.of(2009,10,27),
                List.of(GameGenres.FIGHTING, GameGenres.RPG,GameGenres.ACTION), List.of(windows, xbox),
                List.of(marcMerril, gabeNewell, bobbyKotick));
        Game OW = new Game("Overwatch", 8, LocalDate.of(2016,5,24),
                List.of(GameGenres.ACTION,GameGenres.FIRST_PERSON,GameGenres.SHOOTER), List.of(windows, playstation, xbox, switchConsole),
                List.of(bobbyKotick, adamBoyes,adamKicinski));

        //relations
        gabeNewell.setGames(List.of(CSGO, OW));
        adamKicinski.setGames(List.of(witcher3, OW));
        marcinBlacha.setGames(List.of(CSGO, witcher3));
        marcMerril.setGames(List.of(LOL));
        bobbyKotick.setGames(List.of(LOL, OW));
        adamBoyes.setGames(List.of(OW));

        //add all to repositories
        platformRepository.create(windows);
        platformRepository.create(playstation);
        platformRepository.create(xbox);
        platformRepository.create(switchConsole);

        developerRepositoryImpl.create(gabeNewell);
        developerRepositoryImpl.create(adamKicinski);
        developerRepositoryImpl.create(marcinBlacha);
        developerRepositoryImpl.create(marcMerril);
        developerRepositoryImpl.create(bobbyKotick);
        developerRepositoryImpl.create(adamBoyes);

        gameRepositoryImpl.create(CSGO);
        gameRepositoryImpl.create(witcher3);
        gameRepositoryImpl.create(LOL);
        gameRepositoryImpl.create(OW);
    }
}
