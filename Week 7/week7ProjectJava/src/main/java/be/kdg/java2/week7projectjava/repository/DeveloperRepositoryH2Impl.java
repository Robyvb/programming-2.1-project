package be.kdg.java2.week7projectjava.repository;

import be.kdg.java2.week7projectjava.domain.Developer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Profile("H2")
public class DeveloperRepositoryH2Impl implements DeveloperRepository{

    private final Logger logger = LoggerFactory.getLogger(DeveloperRepositoryH2Impl.class);

    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert insertDeveloper;

    public DeveloperRepositoryH2Impl(JdbcTemplate jdbcTemplate) {
        logger.debug("Creating H2 Developer repository");
        this.jdbcTemplate = jdbcTemplate;
        this.insertDeveloper = new SimpleJdbcInsert(jdbcTemplate).withTableName("DEVELOPERS").usingGeneratedKeyColumns("developerid");
    }

    @Override
    public List<Developer> read() {
        logger.debug("Reading developer H2");
        return jdbcTemplate.query("SELECT * FROM DEVELOPERS", this::mapRow);
    }

    @Override
    public Developer findById(int id) {
        logger.debug("Finding developer by id H2");
        return jdbcTemplate.queryForObject("SELECT * FROM DEVELOPERS WHERE DEVELOPERID = ?", this::mapRow, id);
    }

    @Override
    public Developer create(Developer developer) {
        logger.debug("Creating developer H2");
        Map<String, Object> params = new HashMap<>();
        params.put("DEVELOPERID", jdbcTemplate.queryForObject("SELECT MAX(DEVELOPERID) FROM DEVELOPERS", Integer.class) + 1);
        params.put("FIRST_NAME", developer.getFirstName());
        params.put("LAST_NAME", developer.getSurname());
        params.put("BIRTH_DATE", developer.getBirthDate());
        jdbcTemplate.update("INSERT INTO DEVELOPERS VALUES (?,?,?,?)", params.get("DEVELOPERID"), params.get("FIRST_NAME"), params.get("LAST_NAME"), params.get("BIRTH_DATE"));
        //developer.setId(insertDeveloper.executeAndReturnKey(params).intValue());
        return developer;
    }

    public Developer mapRow(ResultSet rs, int rowNum) throws SQLException{
        logger.debug("Developer mapRow");
        Developer newDeveloper = new Developer(
                rs.getString("FIRST_NAME"),
                rs.getString("LAST_NAME"),
                rs.getDate("BIRTH_DATE").toLocalDate());
        newDeveloper.setId(rs.getInt("DEVELOPERID") + 1);
        return newDeveloper;
    }
}
