package be.kdg.java2.week7projectjava.repository;

import be.kdg.java2.week7projectjava.domain.Game;

import java.util.List;

public interface GameRepository {
    List<Game> read();
    Game findById(int id);
    Game create(Game game);
}
