package be.kdg.java2.week7projectjava.presentation;

import be.kdg.java2.week7projectjava.domain.Developer;
import be.kdg.java2.week7projectjava.domain.Game;
import be.kdg.java2.week7projectjava.presentation.dtos.DeveloperDTO;
import be.kdg.java2.week7projectjava.services.DeveloperService;
import be.kdg.java2.week7projectjava.services.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/developers")
public class DeveloperController{
    private Logger logger = LoggerFactory.getLogger(DeveloperController.class);
    private DeveloperService developerService;
    private GameService gameService;

    public DeveloperController(DeveloperService developerService, GameService gameService){
        logger.debug("Creating developerController");
        this.gameService = gameService;
        this.developerService = developerService;
    }

    @GetMapping
    public String showAllDevelopers(Model model){
        logger.debug("Running showAllDevelopers");
        model.addAttribute("developers", developerService.showAllDevelopers());
        model.addAttribute("games", gameService.showAllGames());
        return "developers";
    }

    @GetMapping("/add")
    public String addDeveloper(Model model){
        logger.debug("User is adding a developer");
        model.addAttribute("games", gameService.showAllGames());
        model.addAttribute("developerDTO", new DeveloperDTO());
        return "addDeveloper";
    }

    @PostMapping("/add")
    public String processAddDeveloper(Model model, @Valid @ModelAttribute("developerDTO") DeveloperDTO developerDTO, BindingResult errors){
        if (errors.hasErrors()){
            logger.debug("User forgot something while adding a developer");
            errors.getAllErrors().forEach(error -> logger.warn(error.toString()));

            //direct games back when error gets shown (else html page doesn't have the games anymore to show)
            model.addAttribute("games", gameService.showAllGames());
            return "addDeveloper";
        } else {
            logger.debug("DTO: " + developerDTO);
            //populate the arrayList with selected games and pass it into a new developer to add to the service class
            List<Game> newGames = new ArrayList<>();
            if(developerDTO.getGames() != null){
                developerDTO.getGames().forEach(id -> newGames.add(gameService.findGameById(id)));
            }
            logger.debug("Games the developer worked on: " + newGames);
            Developer testDeveloper = new Developer(developerDTO.getFirstName(), developerDTO.getSurname(), developerDTO.getBirthDate());
            testDeveloper.setGames(newGames);
            logger.debug("Developer: " + testDeveloper);
            developerService.addDeveloper(testDeveloper);
            return "redirect:/developers";
        }
    }

    @GetMapping("/detailsDeveloper")
    public String detailsDeveloper(@RequestParam("developerID") Integer developerID, Model model){
        //find the selected developer and show it
        Developer requestedDeveloper = developerService.findDeveloperById(developerID);
        model.addAttribute("developer", requestedDeveloper);
        return "detailsDeveloper";
    }
}
