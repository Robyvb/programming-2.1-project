package be.kdg.java2.week7projectjava.repository;

import be.kdg.java2.week7projectjava.domain.Entity;

import java.util.List;

public interface EntityRepository<T extends Entity>{
    List<T> read();

    T create(T t);

    T findById(int id);

    void delete(T t);

    void update(T t);
}
