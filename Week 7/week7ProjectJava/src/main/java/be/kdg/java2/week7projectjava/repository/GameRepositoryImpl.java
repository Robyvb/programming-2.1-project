package be.kdg.java2.week7projectjava.repository;

import be.kdg.java2.week7projectjava.domain.Game;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Profile("collections")
public class GameRepositoryImpl extends ListRepository<Game> implements GameRepository{
    private Logger logger = LoggerFactory.getLogger(GameRepositoryImpl.class);

    public GameRepositoryImpl() {
        logger.debug("Creating game repository");
    }
}
