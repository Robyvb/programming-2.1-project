package be.kdg.java2.week7projectjava.repository;

import be.kdg.java2.week7projectjava.domain.Developer;

import java.util.List;

public interface DeveloperRepository {
    List<Developer> read();
    Developer findById(int id);
    Developer create(Developer developer);
}
