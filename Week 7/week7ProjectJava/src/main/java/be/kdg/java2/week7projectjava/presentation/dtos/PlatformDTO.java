package be.kdg.java2.week7projectjava.presentation.dtos;

import be.kdg.java2.week7projectjava.domain.Game;

import java.util.ArrayList;
import java.util.List;

public class PlatformDTO {
    private String platformName;
    private String abbreviation;
    private String platformMaintainer;
    private int version;

    private List<Game> games = new ArrayList<>();

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getPlatformMaintainer() {
        return platformMaintainer;
    }

    public void setPlatformMaintainer(String platformMaintainer) {
        this.platformMaintainer = platformMaintainer;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    @Override
    public String toString() {
        return "PlatformDTO{" +
                "platformName='" + platformName + '\'' +
                ", abbreviation='" + abbreviation + '\'' +
                ", platformMaintainer='" + platformMaintainer + '\'' +
                ", version=" + version +
                ", games=" + games +
                '}';
    }
}
