package be.kdg.java2.week7projectjava.repository;

import be.kdg.java2.week7projectjava.domain.Developer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Profile("collections")
public class DeveloperRepositoryImpl extends ListRepository<Developer> implements DeveloperRepository{
    private Logger logger = LoggerFactory.getLogger(DeveloperRepositoryImpl.class);

    public DeveloperRepositoryImpl() {
        logger.debug("Creating developer repository");
    }
}
