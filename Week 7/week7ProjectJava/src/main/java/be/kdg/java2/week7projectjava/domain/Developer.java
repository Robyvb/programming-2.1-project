package be.kdg.java2.week7projectjava.domain;

import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Developer extends Entity {
    private String firstName;
    private String surname;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;

    final transient private List<Game> games = new ArrayList<>();

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public String getName(){
        return firstName + " " + surname;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games){
        this.games.addAll(games);
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return surname + " " + firstName + " (born " + birthDate + ")";
    }

    public Developer(String firstName, String surname, LocalDate birthDate) {
        this.firstName = firstName;
        this.surname = surname;
        this.birthDate = birthDate;
    }


}
