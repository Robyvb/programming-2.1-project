package be.kdg.java2.week7projectjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week7ProjectJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(Week7ProjectJavaApplication.class, args);
    }

}
