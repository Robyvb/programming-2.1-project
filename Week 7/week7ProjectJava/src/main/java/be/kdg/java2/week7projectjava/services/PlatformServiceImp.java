package be.kdg.java2.week7projectjava.services;

import be.kdg.java2.week7projectjava.domain.Platform;
import be.kdg.java2.week7projectjava.repository.JSONSaver;
import be.kdg.java2.week7projectjava.repository.PlatformRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlatformServiceImp implements PlatformService{
    private PlatformRepository repository;
    private JSONSaver jsonSaver;
    private static final Logger logger = LoggerFactory.getLogger(PlatformServiceImp.class);

    @Autowired
    public PlatformServiceImp(PlatformRepository repository) {
        this.repository = repository;
    }

    @Override
    public void savePlatformsToJson(List<Platform> platforms) {
        jsonSaver.savePlatformsToJson(platforms);
    }

    @Override
    public List<Platform> showAllPlatforms() {
        return repository.read();
    }

    @Override
    public Platform addPlatform(Platform platform) {
        return repository.create(platform);
    }

    @Override
    public Platform findPlatformById(int id) {
        logger.debug("Finding platform by id " + id);
        return repository.findById(id);
    }
}
