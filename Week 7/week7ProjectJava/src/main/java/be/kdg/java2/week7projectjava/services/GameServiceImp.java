package be.kdg.java2.week7projectjava.services;

import be.kdg.java2.week7projectjava.domain.Game;
import be.kdg.java2.week7projectjava.repository.GameRepository;
import be.kdg.java2.week7projectjava.repository.GameRepositoryImpl;
import be.kdg.java2.week7projectjava.repository.JSONSaver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GameServiceImp implements GameService{
    private GameRepository repository;
    private JSONSaver jsonSaver;
    private static final Logger logger = LoggerFactory.getLogger(DeveloperServiceImp.class);
    @Autowired
    //JSON saving goes here too
    public GameServiceImp(GameRepository repository){
        this.repository = repository;
    }

    @Override
    public void saveGamesToJson(List<Game> games){
        jsonSaver.saveGamesToJson(games);
    }

    @Override
    public List<Game> showAllGames() {
        return repository.read();
    }

    @Override
    public Game addGame(Game game) {
        logger.debug("Adding game:" + game.getName());
        return repository.create(game);
    }

    @Override
    public Game findGameById(int id){
        logger.debug("Finding game by id " + id);
        return repository.findById(id);
    }
}
