package be.kdg.java2.week7projectjava.configuration;

import be.kdg.java2.week7projectjava.converters.StringToGameGenreConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {
    @Override
    public void addFormatters(FormatterRegistry registry){
        registry.addConverter(new StringToGameGenreConverter());
    }
}
