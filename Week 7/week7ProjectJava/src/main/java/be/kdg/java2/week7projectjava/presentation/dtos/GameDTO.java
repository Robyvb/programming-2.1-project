package be.kdg.java2.week7projectjava.presentation.dtos;

import be.kdg.java2.week7projectjava.domain.GameGenres;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class GameDTO {
    @NotEmpty(message = "Please fill in a name")
    @Size(max = 100, message = "Name can be max 100 length")
    private String name;

    //@NotBlank(message = "Please fill in a rating")
    @DecimalMin(value = "0.1",message = "Please fill in a rating")
    private double rating;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "Please fill in release date")
    private LocalDate releaseDate;

    //uncomment for w9 --> many to many
    //@NotEmpty(message = "Please fill in at least one genre")
    private List<GameGenres> genres;

    //uncomment for w9 --> many to many
    //@NotEmpty(message = "Please fill in at least one platform")
    transient List<Integer> platforms;

    //uncomment for w9 --> many to many
    //@NotEmpty(message = "Please fill in at least one developer")
    transient List<Integer> developers;

    public String getName() {
        return name;
    }

    public double getRating() {
        return rating;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public List<GameGenres> getGenres() {
        return genres;
    }

    public void setGenres(List<GameGenres> genres) {
        this.genres = genres;
    }

    public List<Integer> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<Integer> platforms) {
        this.platforms = platforms;
    }

    public List<Integer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<Integer> developers) {
        this.developers = developers;
    }

    @Override
    public String toString() {
        return "GameDTO{" +
                "name='" + name + '\'' +
                ", rating=" + rating +
                ", releaseDate=" + releaseDate +
                ", genres=" + genres +
                ", platforms=" + platforms +
                ", developers=" + developers +
                '}';
    }
}
