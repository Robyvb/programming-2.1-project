package be.kdg.java2.week7projectjava.repository;

import be.kdg.java2.week7projectjava.domain.Platform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PlatformRepository extends ListRepository<Platform>{
    private Logger logger = LoggerFactory.getLogger(PlatformRepository.class);

    public PlatformRepository() {
        logger.debug("Creating platform repository");
    }
}
