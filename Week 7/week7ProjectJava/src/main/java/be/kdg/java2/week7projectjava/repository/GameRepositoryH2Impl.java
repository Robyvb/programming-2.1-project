package be.kdg.java2.week7projectjava.repository;

import be.kdg.java2.week7projectjava.domain.Game;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Profile("H2")
public class GameRepositoryH2Impl implements GameRepository{
    private final Logger logger = LoggerFactory.getLogger(GameRepositoryH2Impl.class);

    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert insertGame;

    public GameRepositoryH2Impl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.insertGame = new SimpleJdbcInsert(jdbcTemplate).withTableName("GAMES").usingGeneratedKeyColumns("GAMEID");
    }


    @Override
    public List<Game> read() {
        return jdbcTemplate.query("SELECT * FROM GAMES", this::mapRow);
    }

    @Override
    public Game findById(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM GAMES WHERE GAMEID = ?", this::mapRow, id);
    }

    @Override
    public Game create(Game game) {
        Map<String, Object> params = new HashMap<>();
        params.put("GAMEID", jdbcTemplate.queryForObject("SELECT MAX(GAMEID) FROM GAMES", Integer.class) + 1);
        params.put("NAME", game.getName());
        params.put("RATING", game.getRating());
        params.put("RELEASE_DATE", game.getReleaseDate());
        jdbcTemplate.update("INSERT INTO GAMES VALUES (?,?,?,?)", params.get("GAMEID"), params.get("NAME"), params.get("RATING"), params.get("RELEASE_DATE"));
        //game.setId(insertGame.executeAndReturnKey(params).intValue());
        return game;
    }

    private Game mapRow(ResultSet rs, int rowNum) throws SQLException{
        Game newGame = new Game(
                rs.getString("NAME"),
                rs.getDouble("RATING"),
                rs.getDate("RELEASE_DATE").toLocalDate());
        newGame.setId(rs.getInt("GAMEID") + 1);
        return newGame;
    }
}
