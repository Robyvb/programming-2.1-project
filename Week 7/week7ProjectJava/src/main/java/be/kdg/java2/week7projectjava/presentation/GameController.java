package be.kdg.java2.week7projectjava.presentation;

import be.kdg.java2.week7projectjava.domain.*;
import be.kdg.java2.week7projectjava.presentation.dtos.GameDTO;
import be.kdg.java2.week7projectjava.services.DeveloperService;
import be.kdg.java2.week7projectjava.services.GameService;
import be.kdg.java2.week7projectjava.services.PlatformService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/games")
public class GameController {
    private Logger logger = LoggerFactory.getLogger(GameController.class);
    private GameService gameService;
    private DeveloperService developerService;
    private PlatformService platformService;

    public GameController(GameService gameService, DeveloperService developerService, PlatformService platformService) {
        logger.debug("Creating gameController");
        this.gameService = gameService;
        this.developerService = developerService;
        this.platformService = platformService;
    }

    @GetMapping
    public String showAllGames(Model model){
        logger.debug("Running showAllGames");
        model.addAttribute("games", gameService.showAllGames());
        return "games";
    }

    @GetMapping("/add")
    public String addGame(Model model){
        logger.debug("User is adding a game");
        model.addAttribute("genres", GameGenres.values());
        model.addAttribute("developers", developerService.showAllDevelopers());
        //logger.debug("Platforms: " + platformService.showAllPlatforms());
        model.addAttribute("platforms", platformService.showAllPlatforms());
        model.addAttribute("gameDTO", new GameDTO());
        return "addGame";
    }

    @PostMapping("/add")
    public String processAddGame(Model model, @Valid @ModelAttribute("gameDTO") GameDTO gameDTO, BindingResult errors){
        if (errors.hasErrors()){
            logger.debug("User forgot something while adding a game");
            errors.getAllErrors().forEach(error -> logger.warn(error.toString()));

            //add values back
            model.addAttribute("genres", GameGenres.values());
            model.addAttribute("developers", developerService.showAllDevelopers());
            model.addAttribute("platforms", platformService.showAllPlatforms());
            return "addGame";
        } else {
        logger.debug("Adding a game: " + gameDTO);
        List<Developer> newDevelopers = new ArrayList<>();
        if(gameDTO.getDevelopers() != null){
            gameDTO.getDevelopers().forEach(id -> newDevelopers.add(developerService.findDeveloperById(id)));
        }
        List<Platform> newPlatforms= new ArrayList<>();
        if(gameDTO.getPlatforms() != null){
            gameDTO.getPlatforms().forEach(id -> newPlatforms.add(platformService.findPlatformById(id)));
        }
        logger.debug(gameDTO.getName() + " developed by: " + newDevelopers);
        logger.debug("Genres: " + gameDTO.getGenres());
        Game newGame = new Game(gameDTO.getName(),gameDTO.getRating(), gameDTO.getReleaseDate(), gameDTO.getGenres(), newPlatforms);
        newGame.setDevelopers(newDevelopers);
        logger.debug("New game added: " + newGame);
        gameService.addGame(newGame);
        return "redirect:/games";
        }
    }


    @GetMapping("/detailsGame")
    public String detailsDeveloper(@RequestParam("gameID") Integer gameID, Model model){
        Game requestedGame = gameService.findGameById(gameID);
        model.addAttribute("game", requestedGame);
        return "detailsGame";
    }
}
