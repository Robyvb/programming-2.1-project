INSERT INTO GAME_GENRES(GENREID, NAME) VALUES
                                           (0, 'Action'),
                                           (1, 'Adventure'),
                                           (2, 'RPG'),
                                           (3, 'Fighting'),
                                           (4, 'Shooter'),
                                           (5, 'First person');

INSERT INTO DEVELOPERS(DEVELOPERID, FIRST_NAME, LAST_NAME, BIRTH_DATE) VALUES
                                                                           (0, 'Gabe', 'Newell', '1962-10-03'),
                                                                           (1, 'Adam', 'Kicinski', '1969-04-21'),
                                                                           (2, 'Marcin', 'Blacha', '1972-11-10'),
                                                                           (3, 'Marc', 'Merrill', '1980-08-17'),
                                                                           (4, 'Bobby', 'Kotick', '1963-10-12'),
                                                                           (5, 'Adam', 'Boyes', '2016-08-08');

INSERT INTO GAMES(GAMEID, NAME, RATING, RELEASE_DATE) VALUES
                                                          (0, 'Counter-Strike: Global Offensive', 9.5, '2012-08-21'),
                                                          (1, 'The Witcher 3', 9.5, '2015-05-18'),
                                                          (2, 'League Of Legends', 9.5, '2009-10-27'),
                                                          (3, 'Overwatch', 9.5, '2016-05-24');

INSERT INTO PLATFORMS(PLATFORMID, NAME, ABBREVIATION, PLATFORM_MAINTAINER, VERSION) VALUES
                                                                                       (0, 'Windows', 'WIN', 'Microsoft', 10),
                                                                                       (1, 'Playstation', 'PS', 'Sony', 5),
                                                                                       (2, 'XBOX', 'XB', 'Microsoft', 5),
                                                                                       (3, 'Switch', 'SW', 'Nintendo', 1);